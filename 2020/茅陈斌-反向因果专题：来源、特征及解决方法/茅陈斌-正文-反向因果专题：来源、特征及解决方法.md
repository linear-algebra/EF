# 反向因果专题：来源、特征及解决方法

&emsp;

> **作者：** 茅陈斌 (中山大学)  
> **邮箱：** <maochb@mail2.sysu.edu.cn> 

&emsp;

&emsp;

---

**目录**
[TOC]

---

&emsp;

&emsp;

## 1. 背景介绍
许多实证研究致力于回答一些有关因果关系的问题：低薪酬会导致职业女性化吗？有限的经济资源是否增加了离婚的风险？社会交往对劳动力市场的影响是什么？好的制度能够带来更高的经济增长率吗？企业创新投入的增加是否能够增加企业价值？这类问题被称作因果推断问题。一直以来，随机实验被认为是因果推断的“黄金法则”。然而，随机试验往往很难应用到社会科学研究中，因为与人类行为及其后果相关的许多有趣的变量 (如工作条件、家庭或社会关系等) 很难被操控。因此，学者们不得不利用观察数据来进行因果推断。

在因果推断中，**反向因果问题** ( reverse causality ) 一直是一个不可忽视的问题。当重新考虑上文提及的研究问题时，人们可能会问：职业的女性化会降低薪酬吗？夫妻是否会因为预料到婚姻中出现的问题而调整他们的工作行为？成功的人会更倾向于进行社会交往吗？经济更发达的地区拥有更好的制度吗？价值高的企业更倾向于增加创新投入吗？从上面的例子中我们可以看到，在许多问题中因果关系往往是双向的。因此，在考虑反向因果问题的基础上建立因果关系并进行分析是因果推断的关键挑战。

即使我们明确了反向因果问题的重要性，在因果推断中准确地识别和估计反向因果关系也绝非易事。基于以上背景，本文从反向因果问题的来源与特征出发，结合论文中的具体实例探究反向因果问题的解决办法。

本文余下部分的安排如下：在第二部分中，本文基于面板数据介绍了反向因果问题的来源与特征；在第三部分中，本文介绍了应对反向因果问题的几种解决办法，并对每种解决办法都列举了论文实例进行说明；在第四部分中，本文进行了总结。

&emsp;

## 2. 反向因果问题的来源与特征
反向因果问题为何会成为因果推断中不可忽视的问题？主要是因为当反向因果关系存在但模型中没有将其考虑在内时，反向因果问题就会使得模型的估计结果出现偏误。下面，本文将基于面板数据来具体说明偏误产生的原因。

在面板数据下，我们想要探究一系列解释变量 _X_ 对被解释变量 _Y_ 的影响。为了估计影响的大小，常用的方法有 POLS、FE 模型、RE 模型、FD 模型四种。 

### 2.1 POLS 方法
首先，我们考虑运用 PLOS 方法进行估计的情况。在此情况下，我们假设回归方程为：

$$y_{it}=\beta_{1}x_{it}+\beta_{2}Z_{i}+\epsilon_{it}$$

其中，$y_{it}$ 为被解释变量，$x_{it}$ 为解释变量，$Z_{i}$ 为不随时间变化的个体特征，$\epsilon_{it}$ 为满足独立同分布的误差项。为了使估计得到的参数具备无偏性，回归方程中的误差项需要满足同期外生性，即：

$$E\left(\epsilon_{it}|x_{it},Z_{i}\right)=0$$

在 POLS 方法下，当允许反向因果关系存在时，只有当所有同时对 _X_ 和 _Y_ 都产生影响的变量都被捕捉到时，参数估计才是有效的。但这样严格的条件在实证研究中往往难以满足。因此，难以捕捉到的个体特征使得同期外生性的假设难以被满足，估计偏误由此产生。

### 2.2 FE 和 RE 模型
我们可以将误差项进一步分解为代表个体特征的误差项 $\alpha_{i}$ 和同时随个体和时间变化的误差项 $\epsilon_{it}$。 根据对 $\alpha_{i}$的处理方式的不同，我们可以得到两类模型：FE ( 固定效应 ) 模型和 RE ( 随机效应 ) 模型。

在 FE 模型下，我们假设回归方程为：

$$y_{it}=\alpha_{i}+\beta_{1}x_{it}+\beta_{2}Z_{i}+\epsilon_{it}$$

其中，FE 模型允许代表个体特征的误差项 $\alpha_{i}$ 与解释变量相关。在 FE 模型下，未被捕捉到的同时对 _X_ 和 _Y_ 都产生影响的变量被包含在 $\alpha_{i}$ 中，因此不会造成偏误。

在 RE 模型下，我们假设回归方程为：

$$y_{it}=\alpha_{i}+\beta_{1}x_{it}+\beta_{2}Z_{i}+\epsilon_{it}$$

其中，RE 模型将代表个体特征的误差项视作与解释变量不相关且服从分布 $N\sim(0;\sigma^{2}_{\alpha})$ 的随机变量。因此，RE 模型允许对不随时间变化的变量进行估计，但需要满足额外的外生性条件，即：

$$E\left(\alpha_{i}|x_{it},Z_{i}\right)=0$$

尽管 FE 和 RE 模型在处理只随时间变化的误差项 $\alpha_{i}$ 的方式上有所不同，但他们都需要满足严格外生性的假设：

$$E\left(\epsilon_{is}|x_{it},\alpha_{i}\right)=0\quad for\ all\ s,t = 1,\cdots,T$$

当反向因果关系存在时，严格外生性的假设往往不能得到满足，这也是偏误产生的原因。严格外生性要求误差项 $\epsilon_{it}$ 和任意时期的解释变量都不相关。若反向因果关系存在，即 $y_{it}$ 影响 $x_{it+1}$，则 $\epsilon_{it}$ 将不可避免地与 $x_{it+1}$ 相关。此时，严格外生性的假设不再满足，故 FE 模型和 RE 模型得到的结果都会出现偏误。

### 2.3 FD模型
在 FD 模型的基础上，我们可以将原回归方程减去个体水平上的均值，消去 $\alpha_{i}$ 和 $Z_{i}$，再对参数进行估计。假设两个时点上的回归方程分别为：

$$y_{it}=\alpha_{i}+\beta_{1}x_{it}+\beta_{2}Z_{i}+\epsilon_{it}$$

$$y_{it-1}=\alpha_{i}+\beta_{1}x_{it-1}+\beta_{2}Z_{i}+\epsilon_{it-1}$$

此时，将两个式子相减可以消去代表个体特征的误差项 $\alpha_{i}$ 和不随时间变化的解释变量 $Z_{i}$：

$$\Delta y_{it}=\beta_{1} \Delta x_{it}+\Delta \epsilon_{it}$$

与 FE 模型和 RE 模型相比相比，FD 模型放松了对代表个体特征的误差项 $\alpha_{i}$ 的外生性假设，但仍需要有关误差项 $\epsilon_{it}$ 的严格外生性假设以得到无偏估计。因此，与 FE 模型和 RE 模型类似，反向因果关系的存在仍会使 FD 模型的估计结果出现偏误。

综上，反向因果问题给面板数据下各个估计方法/模型的估计结果带来了偏误，其主要原因是打破了同期外生性或是严格外生性的假设。接下来，本文将结合论文中的具体实例来探究反向因果问题的解决办法。

## 3. 反向因果问题的解决方法
在第2节中，本文介绍了反向因果问题的来源与特征。接下来，本文将介绍几种反向因果问题的解决办法，并辅以论文实例进行说明。本文主要介绍的方法包括：(1) 解释变量滞后项的引入及LFD模型；(2) 被解释变量滞后项的引入 ( LDV 模型 ) 和 AB 模型；(3) 交叉滞后的固定效应模型( The cross-lagged panel model with FE ) 和 ML-SEM 方法；(4) 影响渠道/机制分析。

### 3.1 解释变量滞后项的引入及 LFD 模型
既然反向因果关系可能导致 $\epsilon_{it}$ 与 $x_{it+1}$ 相关，那么也许 $\epsilon_{it-1}$ 与 $x_{it+1}$ 就不具备这种相关关系。因此，用解释变量的滞后项代替解释变量进入回归可能可以解决反向因果问题。假设此时的回归方程如下：

$$y_{it}=\beta x_{it-1}+\alpha_{i}+\epsilon_{it}$$

尽管解释变量滞后项的引入有助于摆脱强而不可测的严格外生性假设，但它引入了同样强而不可测的假设，即未观测变量序列不相关。由此可见，在 FE 模型和 RE 模型中简单地引入解释变量的滞后项并不能很好地解决反向因果问题。

进一步，我们可以在引入解释变量滞后项的基础上使用一阶差分的方法得到 LFD 模型。假设两个时点上的回归方程分别为：

$$y_{it}=\alpha_{i}+\beta_{1}x_{it-1}+\beta_{2}Z_{i}+\epsilon_{it}$$

$$y_{it-1}=\alpha_{i}+\beta_{1}x_{it-2}+\beta_{2}Z_{i}+\epsilon_{it-1}$$

此时，将两个式子相减可得：

 $$y_{it}-y_{it-1}=\beta (x_{it-1}-x_{it-2})+(\epsilon_{it}-\epsilon_{it-1})$$

 LFD 模型允许 _X_ 影响 _Y_ 时因果反馈过程的存在，即允许$x_{it}$ 和 $epsilon_{it}$ 的未来值之间存在相关性。然而，LFD模型对因果关系描述的准确性要求很高。LFD 模型建立在一个关键的假设上：_Y_ 在两个时点间的变化量是 _X_ 在先前两个时点间变化量的函数。因此，若现实中的因果间的滞后关系并非如模型所示 ( 如 _X_ 对 _Y_ 的影响在同期体现 )，则LFD 模型的估计结果就会产生偏误。

### 3.2 被解释变量滞后项的引入 (LDV 模型) 和AB模型
在回归模型的右边引入被解释变量的滞后项的方法被称为 LDV 模型。我们假设回归方程为：

$$y_{it}=\beta_{1} y_{it-1}+ \beta_{2}x_{it}+\alpha_{i}+\epsilon_{it}$$

此时，被解释变量滞后项的引入导致误差项 $\epsilon_{is}$ 与被解释变量的滞后项之间存在相关性，违背了严格外生性的假设，因而造成偏误。

在 LDV 模型的基础上，做一阶差分即可得到 AB 模型。差分后的回归方程为：

$$\Delta y_{it}=\beta_{1} \Delta y_{it-1}+\beta_{2} \Delta x_{it}+\Delta \epsilon_{it}$$

接着，我们可以利用 $y_{it-2}$ 等被解释变量的更高阶滞后项或 $\Delta y_{it-2}$ 等被解释变量的更高阶滞后项的差分项 来作为 $\Delta y_{it-1}$ 的工具变量。解释变量的处理方法与被解释变量相似。在此基础上，我们可以利用标准 GMM 或系统 GMM 等方法进行估计。

在 AB 模型下，解释变量被假设为是预先决定的 ( predetermined ) ，即满足序列外生性 ( sequentially exogenous )。故原先的严格外生性假设被放松为：

$$E(\epsilon_{is}|x_{it})=0,t\leq s$$

由前文所知，若反向因果关系存在，即 $y_{it}$ 影响 $x_{it+1}$，则 $\epsilon_{it}$ 将不可避免地与 $x_{it+1}$ 相关。此时，严格外生性的假设被打破，即：

$$E(\epsilon_{it}|x_{it+1})\neq 0$$

由此可见，在 AB 模型下，反向因果关系的存在并不会影响放松后的外生性假设。也就是说，AB 模型放松了部分解释变量 ( 被假设为预先决定的解释变量 ) 的外生性假设，从而使得当反向因果关系存在的情况下模型仍能给出一致的估计。

但是，AB 模型也存在一些弊端：(1) 当面对数量较多的矩条件时，AB 模型的估计结果会存在向下偏误和弱工具变量问题；(2) AB 模型在有限样本下表现较差，需要较多的样本个数。

### 3.3 交叉滞后的固定效应模型(The cross-lagged panel model with FE) 和 ML-SEM 方法
在3.1节和3.2节中，我们分别介绍了解释变量滞后项的引入和被解释变量滞后项的引入来作为反向因果问题的解决办法。自然地，我们考虑同时将解释变量和被解释变量的滞后项引入回归模型。
交叉滞后的固定效应模型( The cross-lagged panel model with FE ) 是解决反向因果问题的又一利器。我们假设回归方程如下所示：

$$y_{it}=\beta_{1} y_{it-1}+\beta_{2} x_{it-1} + \beta_{3} Z_{i} + \alpha_{i} + \epsilon_{it}$$

在此基础上，有学者利用 ML-SEM 方法 ( 最大似然估计+结构方程模型 ) 进行估计。与 LFD 模型和 AB 模型类似，ML-SEM 方法通过假设解释变量的序列外生性来允许反向因果关系的存在，即允许误差项 $\epsilon_{it}$ 与解释变量的未来值相关。以 $T=4$ 为例，其回归方程可写成：

$$y_{4}=\beta_{1} y_{1}+\beta_{2} y_{2}+\beta_{3} y_{3}+\delta_{1} x_{1}+\delta_{2} x_{2}+\delta_{3} x_{3}+\gamma Z_{i}+\alpha_{i}+\epsilon_{it}$$

此时，反向因果关系体现为被设为预先决定的解释变量和前一期被解释变量中的误差项的相关关系 ( 如 $x_{3}$ 和 $\epsilon_{2}$ )。同时，未被设为预先决定的解释变量仍可以被认为与任何时点的被解释变量均不相关 (即严格外生性)。此外，该模型也可以估计不随时间变化的变量的参数。值得注意的是，ML-SEM 方法的使用需要建立在误差限序列无关的假设之上。此外，ML-SEM 方法同样面临与 LFD 模型类似的“滞后关系识别问题”。

Maghyereh and Abdoh ( 2020 ) 在讨论油价变动的不确定性和企业投资间的关系时考虑了反向因果问题。作者利用交叉滞后的固定效应模型进行估计。回归方程如下所示：

$$INV_{it}=\beta_{0}+\beta_{1}INV_{it-1}+\beta_{2}OVol_{t-1} + \sum_{k=1}^{K}\theta_{k}X_{it-1}^{k}+  \sum_{m=1}^{M}\theta_{m}X_{t-1}^{m}+\sum_{r=1}^{R}\gamma_{r}D_{rt}+\tau_{i}+\delta_{t}+\epsilon_{it}$$

其中，$INV_{it}$ 表示企业投资水平，$OVol_{t}$ 表示原油价格的不确定性 (波动性)，$X_{it}^{k}$ 表示企业层面的控制变量，$X_{t}^{m}$ 表示控制了金融市场不确定性的、随时间变化的变量，$D_{rt}$ 是表示原油价格不确定性结构性冲击的虚拟变量，$\tau_{i}$ 表示不随时间变化的企业层面个体效应，$\delta_{t}$ 表示时间趋势，$\epsilon_{it}$ 是服从正态分布 $N \sim(0,\sigma^{2})$ 且独立同分布的误差项。

在此基础上，作者还将所有的解释变量滞后一期来控制可能的内生性问题和反向因果问题。作者利用系统 GMM 方法进行估计，并利用两阶段最小二乘 (two-stage least squares) 进行稳健性检验。

为了对回归方程进行估计，作者首先对其进行一阶差分以消去个体效应。接着，作者利用滞后2期至滞后4期的解释变量的水平值作为差分值的工具变量进行估计。为了确保 GMM 估计量的有效性，作者利用过度识别约束检验来检验滞后项作为工具变量是否有效，并利用 Arellano-Bond AR (2) 检验来检验是否存在二阶序列相关。

### 3.4 影响渠道/机制分析
以上三种应对反向因果问题的方法多是在回归方程上做文章，通过滞后项的引入来放松原有的严格外生性假设。此外，也有学者通过影响渠道/机制分析来应对可能存在的反向因果问题。

Pagano and Schivardi ( 2003 ) 分析了企业规模和增长率之间的因果关系。作者利用影响渠道的分析来应对其中可能存在的反向因果问题。本文的基本回归方程为：

$$g_{ij}=\alpha_{0}+\alpha_{1}^{'}X_{ij}+\alpha_{2}ln(S_{ij})+\lambda_{i}+\epsilon_{ij}$$

其中，$g$ 表示国家 $j$ 部门 $i$ 的人均增加值的增长率，$X$ 为控制变量，$S$ 为国家 $j$ 部门 $i$ 的平均企业规模加上1的对数，$\lambda_{i}$ 表示部门的虚拟变量，$\epsilon$ 表示误差项。

接着，作者借鉴 Rajan and Zingales (1998) 的方法对反向因果关系进行了检验。基于基本回归方程，作者将企业的研发强度作为中介变量加入回归方程，具体如下所示：

$$g_{ij}=\theta_{0}+\theta_{1}^{'}X_{ij}+\theta_{2}ln(S_{ij})+\theta_{3}[ln(S_{ij}\times D_{i})]+\lambda_{i}+\epsilon_{ij}$$

其中，$D$ 为研发强度，其捕捉了企业规模对增长率影响的部门差异化效应。若部门差异化效应识别正确且因果关系由企业规模到增长率，则参数 $\theta_{3}$ 应为正且显著。若企业规模通过研发强度影响增长率的论点成立，则我们应该能观测到企业规模和增长率总体相关性估计的减少，即 $\theta_{2}$ 的估计值相对于 $\alpha_{2}$ 的估计值的减少。若研发强度是企业规模对增长率影响的唯一渠道，则 $\theta_{2}$ 应接近于零。

通过检验，作者发现企业规模通过研发强度对增长率产生影响，即基础回归体现的企业规模与增长率的相关关系不是由反向因果关系造成的。

与之类似，许多学者利用渠道/机制的分析来应对反向因果问题：Barsky and Kilian ( 2004 ) 通过分析中介效应 ( 如卡特尔、政治事件、战争、贸易禁令、国际宏观条件等 ) 证明了油价和主要宏观经济变量之间存在反向因果关系；Stijins ( 2005 ) 在计量结果的基础上通过分析影响渠道 (如土地、油气储备、煤炭储备、矿物质储备等) 来分析了自然资源丰富度和经济增长之间的反向因果性。

### 3.5 线性反馈检验 (linear feedback test)

Chong and Calderon ( 2000 ) 利用线性反馈检验 (linear feedback test) 检验了制度有效性和经济增长之间的双向因果关系。借鉴了 Geweke ( 1982 ) 的方法，作者利用线性反馈检验将制度有效性和经济增长之间的相关性分解成三个部分，分别是正向影响 ( from 制度有效性 to 经济增长 )、反向影响 ( from 经济增长 to 制度有效性 ) 和当期影响 ( instantaneous )。线性反馈检验的具体步骤如下所示。

首先，作者考虑了两个回归方程：

$$y_{t}=\sum_{i=1}^{m}\gamma_{2i}x_{t-i}+\sum_{i=1}^m\delta_{2i}y_{t-i}+\xi_{1t}$$

$$x_{t}=\sum_{i=1}^{m}\lambda_{2i}x_{t-i}+\sum_{i=1}^m\phi_{2i}y_{t-i}+\xi_{2t}$$

其中，$x_{t}$ 表示制度有效性的测量值，$y_{t}$ 表示 GDP 的人均增长率。在此基础上，作者还控制了以下变量：初等教育的初始入学率、国内生产总值 ( 期初水平 ) 和区域虚拟变量 ( 拉丁美洲和非洲 )。

基于以上回归，作者得到如下的方差协方差矩阵。其中，$\sum_{ij}=E[\xi_{it},\xi_{jt}^{'}],i,j,=1,2$。

$$
\sum\nolimits_{\xi} = {
\left[
\begin{array}
\sum_{11} & \sum_{12} \\
\sum_{12}^{'} & \sum_{22} \\
\end{array}
\right ]}
$$ 

接着，作者另外考虑了两个回归：

$$y_{t}=\sum_{i=1}^{m}\delta_{1i}y_{t-i}+\varepsilon_{1t}$$

$$x_{t}=\sum_{i=1}^{m}\lambda_{1i}x_{t-i}+\varepsilon_{2t}$$

基于以上回归，作者得到 $\sum_{1}=E[\varepsilon_{1t},\varepsilon_{1t}^{'}]$ 和 $\sum_{2}=E[\varepsilon_{2t},\varepsilon_{2t}^{'}]$。

最后，作者将当期因果也纳入回归方程内：

$$y_{t}=\sum_{i=0}^{m}\gamma_{3i}x_{t-i}+\sum_{i=1}^m\delta_{3i}y_{t-i}+\xi_{1t}$$

$$x_{t}=\sum_{i=1}^{m}\lambda_{3i}x_{t-i}+\sum_{i=0}^m\phi_{3i}y_{t-i}+\xi_{2t}$$

基于以上回归，作者得到 $\sum_{\xi i}=E[\xi_{it},\xi_{it}^{'}],i=1,2$。

在以上计算的基础上，作者利用如下方法测量因果关系：

$$F_{x\rightarrow y}=ln(|\sum\nolimits_{1}|/|\sum\nolimits_{11}|)$$

$$F_{y\rightarrow x}=ln(|\sum\nolimits_{2}|/|\sum\nolimits_{22}|)$$

$$F_{x·y}=ln(|\sum\nolimits_{11}|/|\sum\nolimits_{\xi1}|)=ln(|\sum\nolimits_{22}|/|\sum\nolimits_{\xi2}|)$$

$$F_{x,y}=|\sum\nolimits_{1}|/|\sum\nolimits_{\xi1}|=|\sum\nolimits_{2}|/|\sum\nolimits_{\xi2}|$$

其中，$F_{x,y}$ 表示制度有效性和经济增长之间的线性关系，可以被分解成三个部分：制度有效性对经济增长的影响 $F_{x\rightarrow y}$，经济增长对制度有效性的影响 $F_{y\rightarrow x}$ 和当期影响 $F_{x·y}$。

### 3.6 结构方程组法
若反向因果关系存在，则解释变量和被解释变量之间的关系往往可以用方程组来表示。此时，若只估计方程组中的某一个方程，则会因内生性而造成偏误。因此，有学者利用结构方程组法来应对反向因果问题。

Sridhar et al. ( 2008 ) 探究了电信普及率和经济增长之间的关系。为了应对可能存在的反向因果问题，作者建立了一个内化电信普及率和经济增长的结构方程组。具体来看，作者估计了四个回归方程：

(1) 总回归方程。在总回归方程中，作者将 GDP 的对数值对年度电信投资的对数值、劳动力总量的对数值和电信基础设施存量的对数值进行回归。(2) 需求方程。在需求方程中，作者将电信服务需求的对数值对电信服务价格的对数值 ( 代表价格 ) 和人均实际 GDP 的对数值 ( 代表收入 ) 进行回归。(3) 供给方程。在供给方程中，作者将年度电信投资的对数值对电信服务价格的对数值和市场潜力进行回归。(4) 电信普及率增长方程。 在增长方程中，作者将电信普及率对年度电信投资进行回归。

## 4. 总结
反向因果问题是因果推断中一个不可忽视的问题。反向因果关系的存在。因此，学者们开发了各种模型和方法来应对反向因果问题。本文从反向因果问题的来源和特征出发，介绍了几种常见的应对反向因果问题的方法，并辅以论文实例进行了说明。当然，何种方法是处理反向因果关系的最好方法仍未有定论。学者们仍在不断探索之中。

## 5. 参考资料
- Algan, Yann, Pierre Cahuc, and Andrei Shleifer. 2013. “Teaching Practices and Social Capital.” Sciences Po Publications.
- Bils, Mark Joseph, and Pete Klenow. 2000. “Does Schooling Cause Growth.” The American Economic Review 90 (5): 1160–83.
- Bhargava, Alok, Dean T Jamison, Lawrence J Lau, and Christopher J.L Murray. 2001. “Modeling the Effects of Health on Economic Growth.” Journal of Health Economics 20 (3): 423–40.
- Barsky, Robert B., and Lutz Kilian. 2004. “Oil and the Macroeconomy Since the 1970s.” Journal of Economic Perspectives 18 (4): 115–34.
- Bleakley, Hoyt. 2010. “Malaria Eradication in the Americas: A Retrospective Analysis of Childhood Exposure.” American Economic Journal: Applied Economics 2 (2): 1–45.
- Baier, Scott L., Jeffrey H. Bergstrand, and Michael Feng. 2014. “Economic Integration Agreements and the Margins of International Trade.” Journal of International Economics 93 (2): 339–50.
- Chong, Alberto, and Cesar Calderon. 2000. “Causality and Feedback Between Institutional Measures and Economic Growth.” Economics and Politics 12 (1): 69–81.
- Levine, Ross. 2004. “Finance and Growth: Theory and Evidence.” In National Bureau of Economic Research, 865–934.
- Leszczensky, Woldbring. 2019. "How to Deal With Reverse Causality Using Panel Data?" Recommendations for Researchers Based on a Simulation Study. Sociological Methods and Research.
- Maghyereh, Aktham, and Hussein Abdoh. 2020. “Asymmetric Effects of Oil Price Uncertainty on Corporate Investment.” Energy Economics 86: 104622.
- Pagano, Patrizio, and Fabiano Schivardi. 2003. “Firm Size Distribution and Growth.” The Scandinavian Journal of Economics 105 (2): 255–74.
- Rajan, Zingales. 1998. "Financial Dependence and Growth", The American Economic Review 88: 559-586.
- Rubera, Gaia, and Ahmet H. Kirca. 2012. “Firm Innovativeness and Its Performance Outcomes: A Meta-Analytic Review and Theoretical Integration.” Journal of Marketing 76 (3): 130–47.
- Stijns, Jean-Philippe C. 2005. “Natural Resource Abundance and Economic Growth Revisited.” Resources Policy 30 (2): 107–30.
- Sridhar, Kala Seetharam, and Varadharajan Sridhar. 2008. “Telecommunications Infrastructure and Economic Growth: Evidence from Developing Countries.” Applied Econometrics and International Development 7 (2): 37–56.