- cem,[-link-]( http://gking.harvard.edu/cem/ "-Link-") 
​
- Matching,[-link-]( http://sekhon.berkeley.edu/matching "-Link-")
- MatchIt,[-link-]( http://gking.harvard.edu/matchit "-Link-")
- optmatch,[-link-]( http://cran.r-project.org/web/packages/optmatch/index.html "-Link-") 
- PSAgraphics,[-link-](  http://cran.r-project.org/web/packages/PSAgraphics/index.html "-Link-")
- rbounds,[-link-]( http://cran.r-project.org/web/packages/rbounds/index.html "-Link-") 
- twang, [-link-]( http://cran.r-project.org/web/packages/twang/index.html "-Link-")
​
适用于Stata的软件
- cem, [-link-]( http://gking.harvard.edu/cem/ "-Link-")
- match,[-link-]( http://www.economics.harvard.edu/faculty/imbens/software_imbens "-Link-") 
- pscore,[-link-]( http://www.lrz-muenchen.de/~sobecker/pscore.html "-Link-") 
- psmatch2,[-link-](  http://econpapers.repec.org/software/bocbocode/s432001.htm "-Link-")
- rbounds,[-link-]( http://econpapers.repec.org/software/bocbocode/s438301.htm "-Link-") 
- mhbounds,[-link-]( http://ideas.repec.org/p/diw/diwwpp/dp659.html "-Link-") 
- sensatt, [-link-]( http://ideas.repec.org/c/boc/bocode/s456747.html "-Link-")
​
适用于SAS的软件
​
- SAS usage note,[-link-]( http://support.sas.com/kb/30/971.html "-Link-") 
- Greedy 1 : 1 matching,[-link-]( http://www2.sas.com/proceedings/sugi25/25/po/25p225.pdf "-Link-") 
- gmatch macro,[-link-]( http://mayoresearch.mayo.edu/mayo/research/biostat/upload/gmatch.sas "-Link-") 
- Proc assign, [-link-]( http://pubs.amstat.org/doi/abs/10.1198/106186001317114938 "-Link-")
- 1 : 1 Mahalanobis matching within propensity score calipers,[-link-]( www.lexjansen.com/pharmasug/2006/publichealthresearch/pr05.pdf "-Link-") 
- vmatch macro,[-link-]( http://mayoresearch.mayo.edu/mayo/research/biostat/upload/vmatch.sas "-Link-") 
- Weighting,[-link-]( http://www.lexjansen.com/wuss/2006/Analytics/ANL-Leslie.pdf "-Link-")