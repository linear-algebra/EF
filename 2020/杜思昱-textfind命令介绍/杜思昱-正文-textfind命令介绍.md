&#x1F449;  点击右上方的【编辑】按钮，可以查看 Markdown 原始文档。

&emsp;

> **作者：** 杜思昱 (中山大学)        
> **E-mail:**  <dusy@mail2.sysu.edu.cn>   

&emsp;

---

**目录**
[TOC]

---

&emsp;

本篇推文介绍 Stata 中用于关键词搜索的命令：`textfind `。该命令增强了Stata进行内容分析的能力。除了由字符串函数实现的标准关键词搜索之外，`textfind` 允许用户使用多个关键词和排除标准来识别数据集中的观察结果。


## 1. 引言

`textfind`能够识别、分析并将文本数据转换为分类数据，以便在定量分析中进一步使用。
它使用正则表达式找到一个或多个关键词和排除 (即 **n-grams** 模型)，报告 6 个描述搜索质量的统计量：
+ **Total Finds**：根据特定条件匹配到的观测结果的数量
+ **Average Finds**：观测结果中特定条件的单词数
+ **Average Length**：特定词的文本长度
+ **Average Position**：特定词所在位置
+ **Average tf-idf**：词频-逆文档频率
+ **Means test**：不同搜索标准下的样本 t 检验的 p 值
&emsp;

## 2. 理论基础

### 2.1 算法介绍

#### 2.1.1 n-grams 算法
**n-grams** 是一种基于统计语言模型的算法。它的基本思想是将文本里面的内容按照字节进行大小为 n 的滑动窗口操作，形成了长度为 n 的字节片段序列。

如果我们有一个由 n 个词组成的序列（或者说一个句子），我们希望算得概率：
$$
p(w_1,w_2,...,w_n)
$$

根据链式规则，可得:
$$
p(w_1,w_2,...,w_n)=p(w_1)\times p(w_2|w_1)\times p(w_3|w_1,w_2)\times \cdots \times p(w_n|w_1,w_2,...,w_{n-1})
$$

>但这样的计算方式存在两个明显问题：
    >1.模型参数空间巨大  
    >2.计算的概率数据矩阵严重稀疏（如果其中任何一项概率为 0 ，那么整个联合概率变为 0 ）

为解决该问题，引入**马尔科夫假设** (Markov assumption) 进行改进：当前这个词仅仅跟前面有限个的词相关，不必追溯到最开始的那个词，这样便可以大幅缩减上述算式的长度，即：
$$
p(w_1,w_2,...,w_n)=p(w_i|w_{i-n+1},...,w_{i-1})
$$

条件概率最直接的算法便是计数法，即：
$$
p(w_1,w_2,...,w_n)=\frac {Count(w_{i-n+1},...,w_{i-1},w_i)}{Count(w_{i-n+1},...,w_{i-1})}
$$


#### 2.1.2 tf-idf 算法

**tf-idf** 是文本挖掘和自然语言处理方面相当重要的算法。**tf** 即词频，指某个词在某篇文章中出现的频率。例如，某篇文章共 1000 个词汇，其中“电影”出现 5 次，则其 tf = 5/1000。

对 tf 最直观的理解就是，当一个词在本文中出现的频率越高，则这篇文章的主题和这个词的相关可能性越大。但这样的直观感觉可能会非常不准确，具体来说有两个重要缺陷。

一方面来看，一篇文章中出现最多的字词很可能是<center> **“你”**、**“我”**、**“他”**、**“的”**、**“是”**、**“这”** </center>
等停用词，仅通过这些词来分析一篇文章的内涵是不可行的。所以在文本分析之前需要进行预处理：**去停用词**，也就是把这些在每篇文章里都可能大量出现又和文章意义关联不大的词都去掉。这样做既能减少词汇处理量，又能有效减少歧义。

另一方面，去掉停用词之后的词频是否就能比较准确的表达文章含义了呢？这仍然是不够的。举个例子，如果一篇文章是描述一份国内专利的，那么这篇文章里哪个词汇词频最高呢？很可能就是“**中国**”。这显然与文章的主题“**专利**”不符，但“中国”并不是一个停用词，这个时候就需要另外一个算法：**idf**。

**idf** 即逆文档频率。上面的例子中“**中国**”的词频最高，却不能反应真实的文章内涵，这是为什么呢？很大程度是因为“中国”这个词太常见了，不仅在这篇文章里出现次数多，在其他文章里出现的次数也很多。这说明“中国”这个词不足以描述文章特征。于是评价某个词的独特性的算法 idf 就被设计出来：语料库文章总数/包含某个词的文章数。
$$
idf_i=log\frac {|D|}{|{j:t_i\in d_j}|+1}
$$
>|D| 表示语料库文章总数  
>$t_i$ 表示包含词 $t_i$ 的文章数

这意味着，如果一个词在越多的文章中出现过，那么其独特性就越低。出现的文章数越少，idf值越大，其独特性越高。

>+ 如果一个词在所有语料库的文章中都没出现过，则分母为0，为防止计算错误，往往把分母 + 1；
  >+ 取对数的目的是为了减小词频差异过大带来的 idf 相差过大

最后，根据
$$
tfidf=tf\times idf
$$

选取 tf-idf 值排名前若干的词汇作为一篇文章的主旨，可靠性得以大幅提升。
&emsp;

### 2.2 textfind 语法介绍

基本语法命令如下：
```stata
textfind varlist [if] [in] [, keyword("string1" "string2" ...) but("string1" "string2"...) nocase exact or notable tag(newvar) nfinds length position tfidf]
```

我们来做一些解释：

```keyword ("string1" "string2" ...)``` 是主要的搜索选项，用于在变量集中查找选定的字符串，查找的字符串可以是文本、数字或任何其他 `ustrregexm()` 正则表达式的搜索条件。

>Note:  
  >`regexm` 表示执行一个正则表达式的匹配，如果字符串满足正则表达式，则计算结果为 1; 否则为 0。  
  ustr 表示 unicode string; `ustrregexm` 在 `regexm` 的基础上加强了编码转换 ( unicode )，能够处理其它非普通编码 ASCII 字符，如中文、日语和韩语等。

`but("string1" "string2" ...)` 是主要的排除选项。它在变量集中查找选定字符串并删除其匹配项。

`nocase` 表示不区分大小写。

`exact` 对变量集中的关键词执行精确搜索，只匹配完全等于 "string1"， "string2"…的观测值。

`or` 对关键词中的多个条目执行替代匹配，默认是 “string1” 和 “string2” 的附加搜索。

`notable` 要求 Stata 不要展示统计量表。

`tag(newvar)` 生成一个名为 newvar 的变量，用于标记找到的结果。

`nfinds` 为关键词中的每个字符串生成一个变量，描述字符串在每次观察中的出现次数。默认的变量名是 myvar1_nfind, myvar2_nfind，…

`length` 生成新的变量 myvar_length，描述在变量集中找到的每个变量的单词长度。

`position` 为关键词中的每个字符串生成一个变量，描述每次观测中首次发现字符串的位置。默认变量名是 myvar1_pos, myvar2_pos，…

`tfidf` 为关键词中每个字符串生成一个变量，描述文本在每次观察中的tf-idf逆向文档频率。默认变量名是 myvar1_tfidf, myvar2_tfidf，…
&emsp;


## 3. Stata 实操：分析政府报告

### 3.1 命令安装

```stata
ssc install textfind   ///安装 textfind 命令
```
直接安装，或者

```stata
findit textfind   ///查找 textfind 命令
```

查询相关信息，在以下页面点击 **click here to install** 安装即可
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/textfind%E5%91%BD%E4%BB%A4%E4%BB%8B%E7%BB%8D_Fig4_%E5%AE%89%E8%A3%85_%E6%9D%9C%E6%80%9D%E6%98%B1.png)
&emsp;

### 3.2 导入文本文档
```stata
copy "http://www.gov.cn/guowuyuan/2020zfgzbg.htm" govr.txt  //复制网页内容到本地
shellout "govr.txt" // 打开查看本地的 txt 文档 
```
从政府官网获取 2020 年政府工作报告，复制到本地后打开，得到如下结果：
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/textfind%E5%91%BD%E4%BB%A4%E4%BB%8B%E7%BB%8D_Fig1_%E6%94%BF%E5%BA%9C%E6%8A%A5%E5%91%8A_%E6%9D%9C%E6%80%9D%E6%98%B1.png.png)
可以看到，文档中有很多无关字符，但基本都是英文和符号，并不影响后续中文处理，因此可以不做处理
>Note：若要进行处理，可用 `filefilter` 或 `subinfile` 替换 govr.txt 文档中的无用字符。

报告正文部分如下图所示：
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/textfind%E5%91%BD%E4%BB%A4%E4%BB%8B%E7%BB%8D_Fig2_%E6%94%BF%E5%BA%9C%E6%8A%A5%E5%91%8A%E5%86%85%E5%AE%B9_%E6%9D%9C%E6%80%9D%E6%98%B1.png)

使用 `insheet` 命令，以空格为分隔符，将txt导入 Stata，结果如下图所示，产生 12 个变量，562 个观察值：
```stata
insheet using govr.txt, delimiter(" ")
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/textfind%E5%91%BD%E4%BB%A4%E4%BB%8B%E7%BB%8D_Fig3_%E5%AF%BC%E5%85%A5%E6%96%87%E6%A1%A3_%E6%9D%9C%E6%80%9D%E6%98%B1.png)
&emsp;
### 3.3 关键词查找

经过观察，发现文档主要内容都存在变量 v1 中，所以我们从 v1 里匹配关键词。
```stata
textfind v1, key("财政""经济""教育""医疗""健康""疫情")
```
Stata 生成的结果表如下：
```stata
The following table displays the keyword(s) and exclusion(s) criteria used in
the search and returns six statistics for each variable specified:

Total finds:           the number of observations when criterion is met.
Average finds per obs: the average occurrence of word when criterion is met.
Average length:        the average word length when criterion is met.
Average position:      the average position of match when criterion is met.
Average tf-idf:        the average term frequency-inverse document frequency
                       when the criterion is met.
Means test p-value:    the p-value for a means comparison test across samples
                       identified by the different criteria.

                               Summary Table                               
--------------------------------------------------------------------------------
variable:   v1
n: 562                                       Average                       Means
                    Total   -----------------------------------------       test
keyword(s)          Finds      Finds     Length   Position     TF-IDF    p-value
--------------------------------------------------------------------------------
财政                    4       1.75     150.75       58.5    .055839          .
经济                   25       1.48     106.04      67.84    .052428    .083264
教育                    2        6.5      105.5         82    .266509    .317742
医疗                    2          3        117       66.5    .157223          .
健康                    8       1.25    103.375     58.625    .057632          .
疫情                   15    2.06667    160.667          .    .047356          .
--------------------------------------------------------------------------------
Total                   0          .          .          .          .    .000099
--------------------------------------------------------------------------------
exclusion(s):
```
&emsp;
```stata
textfind v1, key("经济") but("疫情")
```

排除疫情的因素之后，再探索报告中对经济的描述，结果如下：

```stata
 Summary Table                               
--------------------------------------------------------------------------------
variable:   v1
n: 562                                       Average                       Means
                    Total   -----------------------------------------       test
keyword(s)          Finds      Finds     Length   Position     TF-IDF    p-value
--------------------------------------------------------------------------------
经济                   25       1.48     106.04      67.84    .052428          .
--------------------------------------------------------------------------------
Total                  16      1.375    79.8125    51.3125    .072006          0
--------------------------------------------------------------------------------
exclusion(s):
"疫情"
```
&emsp;

### 3.4 结果分析
根据统计结果，我们有以下发现：
“**经济**”和“**疫情**”在 6 个检索词中出现得最多，报告中反复提及。根据 tf-idf 值判断报告的主题， 我们发现“**教育**”和“**医疗**”拥有最高的 tf-idf 值，由此认为 2020 年政府工作报告主题更倾向教育和医疗行业，这或许与新冠疫情的爆发有关。
&emsp;

## 4.结语

本篇推文主要从理论基础和 Stata 实操两方面介绍了 Stata 的命令 —— `textfind` ，它能够识别并统计文本数据,将其转换为分类数据，是文本分析的基础。`textfind` 的语法规则较简单，实际操作也很方便，但其背后蕴藏着大量的自然语言处理和文本分析的逻辑。此外，关键词 `key()` 选项中的内容的多样化选择决定了其广泛的适用性，因此该命令有很高的实用价值。

&emsp;

## 参考文献
- Cox, N. J. 2011. Stata tip 98: Counting substrings within strings. Stata Journal, 11(2): 318-320. [-PDF-] (https://www.stata-journal.com/sjpdf.html?articlenum=dm0056)
- 游万鹏，连享会推文，[Stata: 正则表达式和文本分析] （https://blog.csdn.net/arlionn/article/details/85156842）
