&emsp;

> **作者**：宾健博 (中山大学)    
> **E-Mail:** <binjb@mail2.sysu.edu.cn> 

[TOC]

## 1. 引言

近年来，**贝叶斯估计 (Bayesian estimation)** 在宏观经济分析和复杂的计量经济模型中应用广泛。在主流的经济学期刊中，**VAR (向量自回归)** 模型和 **DSGE (动态随机一般均衡)** 模型是宏观计量经济学的两大基础。而这两个模型在前沿研究中往往都使用贝叶斯方法进行估计。因此掌握贝叶斯方法逐渐成为了每一个宏观计量研究者的必修课。

遗憾的是，由于 Stata 编写相关扩展程序较为不便，一般对 VAR 模型和 DSGE 模型的贝叶斯估计都在 MATLAB (Dynare) 中完成。但尽管如此，我们仍然可以利用 Stata 内置的贝叶斯估计命令处理一些较简单的问题和模型，这不仅免去了在这些问题上繁琐的 MATLAB 编程，也有助于我们学习理解贝叶斯方法的原理。

本文主要介绍贝叶斯估计和 MCMC 方法的基本概念、如何在 Stata 中使用`bayes`命令前缀、标准估计命令`bayesmh`及其自带的交互菜单。本文的安排如下：在第二部分，我将介绍贝叶斯估计的原理；在第三部分，我将通过一个例子来说明其在统计学中的应用；在第四部分，我将介绍 Stata 中贝叶斯估计的具体操作；在第五部分介绍 MCMC 方法和回归模型的贝叶斯估计。

## 2. 贝叶斯估计介绍

无论是统计学还是计量经济学中，**参数估计 (parameter estimation)** 都扮演着非常重要的角色。传统的**频率学派**认为概率是在长期重复的随机实验中产生的性质 (这里的“长期”和经济学中的长期类似，这也意味着我们在理论上永远无法知道准确的概率) 。但在**贝叶斯学派**看来，概率是不确定性的一种表达。这使得我们可以对无法重复的事件的概率进行考察，但相应的问题是将主观性引入了概率之中。反映到参数估计上，频率学派认为模型参数是一个**定值**，我们应当使用观测数据去**接近**它；而贝叶斯学派认为模型参数是一个**随机变量**，有自己的分布，我们应当使用先验信息并结合观测值去**修正**它的**分布**。

因此，贝叶斯估计的核心任务就是结合先验信息和观测值信息，获得模型参数的概率分布 (称为**后验分布 (posterior)**)。我们将所有观测值记为 $\mathbf{y}$，将模型参数记为 $\boldsymbol{\theta}$ **(注意参数可以有多个，在这里统一记为 $\boldsymbol{\theta}$ )**。在估计之前，我们需要确定**先验分布 (prior)** 和**似然函数 (likelihood function)**。其中先验分布 $p(\boldsymbol{\theta})$ 代表了我们的先验信念，即不依赖于观测值的主观看法。比如在抛一枚硬币之前，我们可能会主观认为它取正面的概率是 50%，这就是一种先验信念。而似然函数 $h(\mathbf{y} \mid \boldsymbol{\theta})$ 则代表了一种将观测值转化为概率的 **“模式”**，如果我们将$h(\boldsymbol{\theta} \mid \mathbf{y})$中的 $\boldsymbol{\theta}$ 和 $\mathbf{y}$ 互换，就可以得到给定参数下观测值的概率 $p(\mathbf{y} \mid \boldsymbol{\theta})$ 。同理，我们将 $h(\boldsymbol{\theta} \mid \mathbf{y})$ 理解为给定观测值时参数的概率。对于同一个问题而言，$h(\boldsymbol{\theta} \mid \mathbf{y})$ 和 $p(\mathbf{y} \mid \boldsymbol{\theta})$ 的数学形式是一样的，区别只在于谁为给定谁为未知。

在确定了先验分布和似然函数后，我们可以使用**贝叶斯定理 (Bayes' Law)** 求解出后验分布：

$$
p(\boldsymbol{\theta} \mid \mathbf{y})=
\frac{p(\mathbf{y} \mid \boldsymbol{\theta}) p(\boldsymbol{\theta})}{p(\mathbf{y})} = \frac{h(\boldsymbol{\theta} \mid \mathbf{y}) p(\boldsymbol{\theta})}{p(\mathbf{y})}
$$

其中：
$$
p(\mathbf{y})=\int_{\boldsymbol{\theta}} p(\mathbf{y} \mid \boldsymbol{\theta}) p(\boldsymbol{\theta})
$$

在实际的贝叶斯分析中，由于 $p(\mathbf{y})$ 是常数，因此我们一般忽略这一项，只考虑后验分布的分子，一般写作：

$$
p(\boldsymbol{\theta} \mid \mathbf{y}) \propto h(\boldsymbol{\theta} \mid \mathbf{y}) p(\boldsymbol{\theta})
$$

## 3. 最简单的贝叶斯估计——伯努利实验

在本章中我将介绍贝叶斯估计的实例。作为一个最简单的参数估计的例子，伯努利实验在统计学教科书中也常常用于介绍贝叶斯估计。我们假设现有一枚硬币，不过我们不知道其抛起后正反面朝上的概率，现在需要估计其正面朝上的概率 $\theta$。

根据前面的讨论，我们需要明确先验分布和似然函数。由于似然函数不具有主观性，我们先考察似然函数。似然函数来源于观测数据，假设我们一共抛了 $n$ 次硬币，其中正面朝上的次数为 $a$，我们可以很容易地得出其似然函数应为：

$$
h(\theta \mid \mathbf{y})=\prod_{i=0}^{n} P\left(y_{i} \mid \theta\right)=\theta^{a}(1-\theta)^{n-a}
$$

接下来考虑先验分布的选择。在实际操作中，我们往往选择似然函数的**共轭先验 (conjugate prior)** 。共轭先验具有**与似然函数相乘后分布族保持不变**的特征。这意味着我们可以通过简单的数学推导求解出 $h(\boldsymbol{\theta} \mid \mathbf{y}) p(\boldsymbol{\theta})$。在一些较复杂的问题中，如果不使用共轭先验，可能会导致计算量过大，因此在接下来的例子中我们都会使用共轭先验。在维基百科中可以查询到所有常用似然函数的共轭先验。在本例中，似然函数是由**伯努利分布 (Bernoulli distribution)** 推导而来，因此我们选择其对应的共轭先验——**Beta分布**。

在选定了先验分布的分布族之后，还需要调整分布的参数以符合我们的先验信念。Beta分布有两个参数——$\alpha$ 和 $\beta$，通过改变两者的大小，其形状会发生很大的变化。我们在这里重点关注两种情况：

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/PDF_of_the_Beta_distribution.gif" width = "500" height = "500" div align=center />

* $Beta(1,1)$ ——就是**均匀分布 (uniform distribution)**。由于均匀分布意味着参数在每一个值的概率密度相同，因此也称为**无信息先验 (non-informative prior)** 或者**扁平先验 (flat prior)**。如果选用这个分布，就代表我们完全没有参数的先验信息。

* $Beta(20,20)$。我们可以发现当 $\alpha=\beta>1$ 时，Beta分布会出现“中间高，两头低”的情况，且概率密度的最大值出现在 0.5 处。如果选用这样的分布，意味着我们认为模型参数$\theta=0.5$的可能性最高，往两边递减，这和我们一般的常识相符 (抛硬币正面朝上的概率应该在 50% 附近) 。同时还可以发现 $\alpha$ 和 $\beta$ 越大，概率密度越向中间集中，因此 **$\alpha$ 和 $\beta$ 可以作为衡量我们先验信息确信程度的参数**。在这里选择的20并不是唯一选择，代表了我们对先验信息是比较确信的。

在 Stata 中，我们可以使用上述选择的先验分布对原问题进行估计，具体代码如下：

```Stata
clear //清空工作区
set seed 20 //设定随机种子

set obs 10000 //设定观测值数量，在这里 n=10000

gen y = runiform(0,1)>0.5 //生成服从伯努利分布的样本，记为 y

bayesmh y, likelihood(dbernoulli({theta})) prior({theta}, beta(1,1)) //估计命令
```

运行结果如下：

```
Burn-in ...
Simulation ...

Model summary
------------------------------------------------------------------------------
Likelihood: 
  y ~ bernoulli({theta})

Prior: 
  {theta} ~ beta(1,1)
------------------------------------------------------------------------------

Bayesian Bernoulli model                         MCMC iterations  =     12,500
Random-walk Metropolis-Hastings sampling         Burn-in          =      2,500
                                                 MCMC sample size =     10,000
                                                 Number of obs    =     10,000
                                                 Acceptance rate  =       .442
Log marginal likelihood = -6935.8231             Efficiency       =      .1992
 
------------------------------------------------------------------------------
             |                                                Equal-tailed
             |      Mean   Std. Dev.     MCSE     Median  [95% Cred. Interval]
-------------+----------------------------------------------------------------
       theta |  .4991434   .0051053   .000114    .499113   .4892574   .5092233
------------------------------------------------------------------------------

```

当设定先验分布为 $Beta(1,1)$ 时，平均值的点估计结果为 0.4991434，和真实的数据生成过程中的参数 0.5 很接近，如果我们选择具有较强先验信息的 $Beta(20,20)$ 作为先验，同样的设定下点估计值更加接近 0.5，达到了 0.4992197，这证明了先验信息对后验分布的影响。



## 4. Stata 中的贝叶斯估计操作

在刚刚的例子中我们对贝叶斯估计进行了实践，可以看到 Stata 为简单的贝叶斯估计提供了便捷的操作。上述的例子中，核心代码是`bayesmh y, likelihood(dbernoulli({theta})) prior({theta}, beta(1,1))`。在这一句代码中，`likelihood()`指定了似然函数的形式，本例中似然函数由伯努利分布导出，同时我们将参数称为 theta **(在本例中只有一个参数，因此不需要进行区分，只是单纯的命名，在更复杂的例子中需要通过名字区分参数)**。`prior()`则指定了先验分布，本例中将 theta 的先验分布定为 $Beta(1,1)$。

上述内容的编程实现看起来并不困难，但是在更复杂的问题中，根据似然函数和先验分布的不同，很多时候需要指定额外的参数或选项，这导致贝叶斯估计的主命令`bayesmh`并不适合直接使用，而往往通过 Stata 自带的交互菜单生成。

为了打开贝叶斯估计的交互界面，我们可以点击 Stata 上方的 **Statistics** 菜单，选择底部的 **Bayesian analysis** 选项，再根据对应问题选择相应选项。上文的例子不涉及回归分析，因此应选择 **General estimation and regression**。

由于我们的模型是一个简单的独立实验，每一个观测值都是一次独立实验的结果，因此在 Syntax 处选择 **Univariate distributions**, Dependent variable 处选择我们生成的变量 y。接下来的 Distribution 代表用于确定似然函数的分布，在本例中选择伯努利分布，同时指定参数名为 theta。最后需要指定模型的先验分布，首先点击 Create 按钮，然后在弹出的菜单中选择相应的分布族和参数，便可以生成一个想要的先验分布。最终结果如下：

![界面截图](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E7%95%8C%E9%9D%A2%E6%88%AA%E5%9B%BE.png)

接下来点击 OK 键，系统便会自动开始估计，可以得到第三部分中的结果。

为了说明交互界面的便利性，我们考虑一个稍微复杂一些的例子。假设样本不再是由伯努利分布生成，而是由**二项分布** $B(10,0.3)$生成，每一个观测值代表进行了 10 次成功率为 0.3 的伯努利实验，观测值序列记为 $\mathbf{y}$。我们依旧可以导出其似然函数：( $n$ 代表二项分布的第一个参数，$p$ 代表二项分布的第二个参数)

$$
P(X=x)=C_n^x p^x(1-p)^{n-x} \\
~\\
h(p\mid n, \mathbf{y})=
\prod_{i=1}^{n} C_{n}^{y_{i}} p^{\sum_{i=1}^{n} y_{i}}(1-p)^{n^{2}-\sum_{i=1}^{n} y_{i}}
$$

注意到在这个例子中，原分布和似然函数中有两个参数 $n$ 和 $p$ ，而参数 $n$ 是预先给定的，因此在使用交互界面时需要输入这个参数。沿用之前的先验分布 **(二项分布的共轭先验也是 Beta分布)**，此时的代码、结果和菜单操作如下：

```Stata
clear //清空工作区
set seed 20 //设定随机种子

set obs 10000 //设定观测值数量，在这里 n=10000

gen z = rbinomial(10,0.3) //生成服从伯努利分布的样本，记为 z

bayesmh z, likelihood(dbinomial({theta},10)) prior({theta}, normal(0,10000))
```

```
Burn-in ...
Simulation ...

Model summary
------------------------------------------------------------------------------
Likelihood: 
  z ~ binomial({theta},10)

Prior: 
  {theta} ~ beta(1,1)
------------------------------------------------------------------------------

Bayesian binomial model                          MCMC iterations  =     12,500
Random-walk Metropolis-Hastings sampling         Burn-in          =      2,500
                                                 MCMC sample size =     10,000
                                                 Number of obs    =     10,000
                                                 Acceptance rate  =      .4526
Log marginal likelihood = -17700.763             Efficiency       =      .2339
 
------------------------------------------------------------------------------
             |                                                Equal-tailed
             |      Mean   Std. Dev.     MCSE     Median  [95% Cred. Interval]
-------------+----------------------------------------------------------------
       theta |  .2987419   .0014841   .000031   .2987601   .2959134   .3016467
------------------------------------------------------------------------------

```

![界面截图2](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E7%95%8C%E9%9D%A2%E6%88%AA%E5%9B%BE2.png)

可以看到，当将似然函数选项选为二项分布之后，在菜单界面中多了一个 **Bernoulli trials** 的输入框，也就是上文指定的 $n$。在更复杂的问题和模型中，可能会要求输入更多的参数和其他内容，此时菜单界面可以很好地代替复杂的语句指令，免去了繁杂的语法记忆。

不仅如此，我们还可以通过键入命令`db postest`打开后验分析的交互菜单进行进一步的分析。常用的功能如 **Bayesian analysis** 下的 **Graphical summaryies and convergence diagnostics** 菜单，可以绘制很多与后验分布相关的图，如后验分布的**频率直方图 (Histograms)**
。还有 **Hypothesis testing using model posterior probabilities** 和 **Interval hypothesis testing** 菜单可以利用生成的后验分布进行假设检验，读者可以根据其需求自行操作。

## 5. MCMC 方法和线性回归的贝叶斯估计

在前面的例子中，可能会有读者感到疑惑，当先验分布选为共轭先验时，后验分布是有解析表达式的，这意味着其概率密度函数应该是确定的。但在结果显示的界面中，明显有**模拟 (simulation)**、**迭代 (iteration)** 和 **抽样 (sampling)** 这样的常见于数值方法的字眼。同时在程序开始时我设定了随机种子。这其实是因为 Stata 并非使用解析方法对后验分布进行求解。为了进一步深入了解现代贝叶斯方法的操作流程，我们有必要对 **MCMC (Markov chain Monte Carlo，马尔科夫链蒙特卡洛)** 方法进行介绍和说明。

对于 MCMC 方法，维基百科中这样作出定义：马尔可夫链蒙特卡洛方法包括一类用于从概率分布中采样的算法。通过构建具有所需分布作为其平衡分布的马尔可夫链，可以通过记录链中的状态来获得所需分布的样本。这一段话有些抽象，实际上从应用层面上来说，我们只需要了解两个最常用的 MCMC 方法—— **Metropolis–Hastings 算法 (简称M-H算法)** 和 **吉布斯采样 (Gibbs sampling)**。前者是 Stata 内置的对后验分布进行抽样的算法，其主要作用是通过后验分布的概率密度之比构造了一个具有接受-拒绝机制的马尔科夫链，并通过它抽取出所需的后验分布。这一算法也广泛应用于 DSGE 模型的贝叶斯估计中，但它不是本文的重点。对于 M-H 算法的主要工作原理和具体功能可以参见[维基百科](https://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm)和这一篇介绍的[文章](https://www.sohu.com/a/219025557_100091665?spm=smpc.author.fd-d.11.1610959736519spDXFE9)。

接下来我将介绍在宏观计量经济学中最常用的 (主要是因为其应用于 VAR 模型的贝叶斯估计) MCMC 方法——**吉布斯采样**。为了让大家对吉布斯采样的应用背景有一个简单的了解，接下来以线性回归为例进行介绍。

我们假设有如下的线性回归模型：( $T$ 为观测值个数，$K$ 为回归元个数)

$$
\begin{aligned}
Y_{t} &=X_{t}B+v_{t} \\
v_{t} & \sim N\left(0, \sigma^{2}\right)
\end{aligned}
$$

其中 $Y_t$ 是一个 $T\times1$ 的矩阵，$X_t$ 是一个 $T\times K$的矩阵，$B$ 是一个 $K\times 1$的矩阵，我们可以写出其似然函数的表达式：

$$
h\left(B, \sigma^{2} \mid Y_{t}\right)=\left(2 \pi \sigma^{2}\right)^{-T / 2} \exp \left(-\frac{\left(Y_{t}-X_{t}B\right)^{\prime}\left(Y_{t}-X_{t}B\right)}{2 \sigma^{2}}\right)
$$

注意到这里有两个待估参数——系数矩阵 $B$ 和扰动项方差 $\sigma^2$。我们分别考虑以下三种情况：

1. 系数矩阵 $B$ 未知，扰动项方差 $\sigma^2$ 已知
2. 系数矩阵 $B$ 已知，扰动项方差 $\sigma^2$ 未知
3. 系数矩阵 $B$ 和扰动项方差 $\sigma^2$ 均未知

在**第一种情况**中，由于扰动项方差已知，查阅维基百科的共轭先验表可以知道此时的共轭先验是**正态分布**，因此假定系数矩阵 $B$ 的先验分布为 $B\sim N(B_0,\Sigma_0)$。接下来进行运算：

先验分布：

$$
\begin{aligned}
f(B) &= (2 \pi)^{-K / 2}\left|\Sigma_{0}\right|^{-\frac{1}{2}} \exp \left[-0.5\left(B-B_{0}\right)^{\prime} \Sigma_{0}^{-1}\left(B-B_{0}\right)\right] \\
&\propto \exp \left[-0.5\left(B-B_{0}\right)^{\prime} \Sigma_{0}^{-1}\left(B-B_{0}\right)\right]
\end{aligned}
$$

似然函数：

$$
\begin{aligned}
h\left(B \mid \sigma^{2},Y_{t}\right) &=\left(2 \pi \sigma^{2}\right)^{-T / 2} \exp \left(-\frac{\left(Y_{t}-X_{t} B\right)^{\prime}\left(Y_{t}-X_{t} B\right)}{2 \sigma^{2}}\right) \\
& \propto \exp \left(-\frac{\left(Y_{t}-X_{t} B\right)^{\prime}\left(Y_{t}-X_{t} B\right)}{2 \sigma^{2}}\right)
\end{aligned}
$$

后验分布：

$$
p\left(B \mid \sigma^{2}, Y_{t}\right) \propto \exp \left[-0.5\left(B-B_{0}\right)^{\prime} \Sigma_{0}^{-1}\left(B-B_{0}\right)\right] \times \exp \left(-\frac{\left(Y_{t}-X_{t} B\right)^{\prime}\left(Y_{t}-X_{t} B\right)}{2 \sigma^{2}}\right)
$$

可以发现后验分布仍然是正态分布：

$$
p\left(B \mid \sigma^{2}, Y_{t}\right) \sim N(M^*,V^*) \\
M^{*} &=\left(\Sigma_{0}^{-1}+\frac{1}{\sigma^{2}} X_{t}^{\prime} X_{t}\right)^{-1}\left(\Sigma_{0}^{-1} B_{0}+\frac{1}{\sigma^{2}} X_{t}^{\prime} Y_{t}\right) \\
V^{*} &=\left(\Sigma_{0}^{-1}+\frac{1}{\sigma^{2}} X_{t}^{\prime} X_{t}\right)^{-1}
$$

接下来考虑**第二种情况**，当系数矩阵 $B$ 已知，扰动项方差 $\sigma^2$ 未知。这时由于似然函数中的给定变量改变，其共轭先验也随之发生变化。查表知此时扰动项方差 $\sigma^2$ 应服从**倒伽马分布 (Inverse Gamma distribution)**，具体运算过程和第一种情况类似，可以获得相同分布族的后验分布 $p\left(\sigma^{2} \mid B, Y_{t}\right)$。

在实际情况中，更加常见的是**第三种情况**——两个待估参数都未知。这时没有办法获得单一的共轭先验，一种解决办法是设定联合先验分布：

$$
p\left(B, \sigma^{2}\right)=P\left(\sigma^{2}\right) \times P\left(B \mid \sigma^{2}\right)
$$

其中 $\sigma^2$ 的分布仍是无条件的，和第二种情况中的设定一样，但 $B$ 则是有条件的，我们将其设定为：
$$
P\left(B \mid \sigma^2\right)\sim N\left(B_{0}, \sigma^{2} \Sigma_{0}\right)
$$

在这样的设定下，联合先验分布还是一个正态分布，和似然函数的分布族一致，这种情况我们称之为**自然共轭先验 (natural conjugate prior)**。

需要注意的是，最后解出的后验分布是一个**联合后验分布** $p\left(\sigma^{2}, B\mid Y_{t}\right)$，但我们想考察的后验分布则是**边缘后验分布** $p\left(B \mid \sigma^{2}, Y_{t}\right)$ 和 $p\left(\sigma^{2} \mid B, Y_{t}\right)$。在自然共轭先验的设定下，我们可以通过积分的方法求得边缘后验分布的解析形式。但这也对先验分布的选择施加了很强的限制。在较复杂的模型中，我们往往不希望限制先验分布的选择，但非自然共轭先验难以通过积分求解出边缘后验分布，因此需要应用吉布斯采样。

吉布斯采样的思想其实很简单，当我们有了两个能条件分布的抽取器时 (即第一种和第二种情况)，可以通过迭代的方法获得逼近边缘分布的抽样。在本例中，具体操作为：

1. 设定系数矩阵 $B$ 的无条件先验分布和扰动项方差 $\sigma^2$ 的无条件先验分布，并从$\sigma^2$ 的无条件先验分布中抽取一个样本
2. 根据**第一种情况**的做法，以第 1 步中抽取的 $\sigma^2$ 为条件，从获得的后验分布中抽取一个 $B$ 的样本
3. 根据**第二种情况**的做法，以第 2 步中抽取的 $B$ 为条件，从获得的后验分布中抽取一个 $\sigma^2$ 的样本
4. 重复第 2 步和第 3 步，获得一个经验样本，可用于估计边缘后验分布。

这一操作步骤看似复杂，但在 Stata 中的实现其实很容易，只需要在线性回归的命令前加上`bayes:`即可，但需要注意的是，此时回归命令不能缩写，并且如果需要使用吉布斯采样的话，需要使用`bayes,gibbs:`。

```Stata
clear //清空工作区
set seed 20 //设定随机种子

sysuse auto //使用系统自带的 auto 数据集

bayes,gibbs: regress price mpg weight //执行吉布斯采样的贝叶斯估计
```

结果如下：
```
Burn-in ...
Simulation ...

Model summary
------------------------------------------------------------------------------
Likelihood: 
  price ~ normal(xb_price,{sigma2})

Priors: 
  {price:mpg weight _cons} ~ normal(0,10000)                               (1)
                  {sigma2} ~ igamma(.01,.01)
------------------------------------------------------------------------------
(1) Parameters are elements of the linear form xb_price.

Bayesian linear regression                       MCMC iterations  =     12,500
Gibbs sampling                                   Burn-in          =      2,500
                                                 MCMC sample size =     10,000
                                                 Number of obs    =         74
                                                 Acceptance rate  =          1
                                                 Efficiency:  min =      .9457
                                                              avg =      .9864
Log marginal likelihood = -696.85658                          max =          1
 
------------------------------------------------------------------------------
             |                                                Equal-tailed
             |      Mean   Std. Dev.     MCSE     Median  [95% Cred. Interval]
-------------+----------------------------------------------------------------
price        |
         mpg | -5.282435   27.88942   .278894  -5.410969  -59.53671   49.12283
      weight |  2.074039    .198759   .001988   2.076242   1.682404   2.464258
       _cons |  1.928181   99.76441   .997644    2.99603  -202.2644   194.9716
-------------+----------------------------------------------------------------
      sigma2 |   6448502    1099995   11311.5    6331241    4641478    8928296
------------------------------------------------------------------------------
Note: Default priors are used for model parameters.
```

## 6. 参考资料
* M-H 算法和共轭先验的维基百科
* Stata 自带的`bayes`和`bayesmh`的文档
* Bill Rising, StataCorp LP, *Bayesian Analysis using Stata*, 2015
* Andrew Blake and Haroon Mumtaz, Bank of England, *Applied Bayesian econometrics for central bankers*, 2017
* Gary Koop and Dimitris Korobilis, Foundations and Trends in
Econometrics, *Bayesian Multivariate Time Series Methods for Empirical Macroeconomics*, 2009
