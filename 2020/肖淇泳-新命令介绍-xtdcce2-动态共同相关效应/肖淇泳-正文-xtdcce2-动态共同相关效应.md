# xtdcce2 — 动态共同相关效应

> **作者：** 肖淇泳 (中山大学)  
> **邮箱：** <xiaoqy25@mail2.sysu.edu.cn>
> **Source：** Jan Ditzen, 2018, Estimating Dynamic Common-Correlated Effects in Stata, Stata Journal, 18(3): 585–617. [-PDF-](https://www.sci-hub.ren/10.1177/1536867x1801800306)

&emsp;

---
**目录**
[toc]

---

&emsp;

## 1. 引言

传统的面板数据模型根据个体效应性质可以分为固定效应模型和随机效应模型，根据回归系数是否存在异质性而分为同质性模型和变系数模型。在构建这些面板数据模型时，一个很常见的假设便是假定截面之间是相互独立的 (徐秋华和张梓玚, 2019) 。

然而，**在实际问题中，截面之间的独立性假设常常是不合理的，截面相关在面板数据中经常出现**。根据 Breitung 和 Pesaran (2008) 的研究，**面板数据的特征之一就是其截面之间存在不同程度的相关性，进而产生了截面异质性或回归误差项之间的相关性，从而影响标准面板数据估计量的无偏性、一致性和有效性**。截面相关性问题，愈来愈成为面板数据建模中备受关注的难点问题之一。

在本篇推文中，我们首先会对截面相关及其解决方法进行简单介绍，之后重点阐述解决方法中的共同相关效应模型和动态共同相关效应模型。我们还将介绍相应的 Stata 命令 `xtdcce2`, 最后给出具体的应用实例。

&emsp;

## 2. 理论介绍

### 2.1 截面相关 (Cross-sectional Dependence)

**截面相关**是当前面板数据模型研究的重点，**因子结构和空间相关**是其中两个重要的研究方向 (钱金保, 2013) 。**因子结构**的相关研究认为**不可观测的共同冲击或个体效应导致截面相关**，主要包括共同因子和时变个体效应两种情形；**空间相关**强调**空间内的个体由于经济联系和距离的差异产生不同程度的相互影响**，这种影响通过空间滞后项予以反映。

一般情形下，因子结构是截面强相关，空间相关是截面弱相关 (Pesaran and Tosetti, 2007; Sarafidis and Wansbeek, 2010) 。两种设定出发点不同，各有优劣：因子结构的优点是能够识别出导致波动的共同因子 (或时变个体效应)，但不能直接获悉其背后的经济含义，也不能从中知道波动的传递途径；空间相关的优点是经济含义明确，但空间相关只能是一种随 “ 距离 ” 而衰减的相关，它不能反映共同冲击的影响。

近些年来，截面强相关研究取得许多重大突破。根据研究背景的区别，可将截面强相关归纳为两种类型：第一种类型的背景是**大 N 和大 T** ，相关研究考虑了**共同因子**及其加载系数的估计 (Bai and Ng, 2002; Bai, 2003) ，以及共同因子设定对结构性模型估计的影响 (Coakley et al., 2002; Pesaran, 2006; Kao et al., 2006; Bai, 2009) ；第二种类型的背景是**大N和固定T**，相关研究把普通的固定效应一般化为**时变个体效应**，考虑了时变个体效应下面板模型的估计问题 (Ahn et al., 2001; Han et al., 2005; Lee, 2006) 。

在下一小节中，我们将介绍截面强相关中的共同因子模型。

### 2.2 共同因子模型 (Common Factor Model)

共同因子模型的基本建模思想是使用一个或少数几个变量 (reference variables) 捕捉大量经济变量的共同波动 (Bai and Ng, 2002) 。共同因子模型在资产定价理论、商业周期研究等领域有着广泛的应用。在这些领域中，共同因子被用于反映要素回报、共同冲击等不可观测因素 (Bai, 2003) 。

对于共同因子的研究，主要分为两类：**第一类研究通常不直接估计共同因子本身，而是通过寻找代理变量，消除共同因子对估计结果的影响**；**第二类研究直接估计共同因子，并把它们作为回归方程的解释变量** (钱金保，2013) 。与第二类研究相比，**第一类研究不需要估计共同因子，估计方法更简单，因此有着更为广泛的应用**。

本篇推文所要讲述动态共同相关效应模型，就属于第一类研究，也是这个方向最为前沿的进展。在我们开始最终的屠龙之旅前，还是先看一下动态共同相关效应 (Dynamic Common Correlated Effects, DCCE) 模型的前身 —— 共同相关效应 (Common Correlated Effects, CCE) 模型。

### 2.3 共同相关效应（Common Correlated Effects)

在 Pesaran (2006) 提出大名鼎鼎的共同相关效应估计之前，已经有一些学者针对截面相关中的共同因子展开了研究：Phillips 和 Sul (2003) 提出一个含有共同因子的自回归模型，然后利用似无相关方法进行估计。然而，Phillips 和 Sul (2003) 既没有包含外生变量，也没有给出渐进结果；Coakley 等 (2002) 指出，OLS 估计残差的主成分可以作为不可观测共同因子的代理变量，但 Pesaran (2006) 指出当共同因子和自变量相关时，该估计不一致。

针对上述问题，**Pesaran (2006) 利用可观测变量 (解释变量和被解释变量) 的截面均值作为不可观测因子的代理变量，通过 OLS 辅助回归得到共同相关效应估计估计**。Pesaran (2006) 也推导和给出了共同相关效应估计的渐进分布，并通过模拟考虑了它们的小样本表现。

以下是具体的共同相关效应模型推导：

$y_{it}$ 表示在时间 $t$ 从第 $i$ 个个体得到的变量 $y$ 的观测值，$i=1,2,...,N;t=1,2,...T$ 。参考Pesaran (2006) ，设定如下线性面板模型：

$$
y_{it}=\alpha_{i}+\beta_{i}^{T}x_{it}+\gamma_{i}^{T}f_{t}+e_{it}\quad(1)
$$

$$
x_{it}=\Gamma_{i}^{T}f_{t}+\varepsilon_{it}\quad(2)
$$

其中，$f_{t}$ 表示不可观测的共同因子，允许 $f_{t}$ 与 $x_{it}$ 相关；$\gamma_{i}$，$A_{i}$ 和 $\Gamma_{i}$ 表示各自对应的共同因子的加载系数；$x_{it}$ 表示解释变量，$\beta_{i}$ 表示待估计系数，$\alpha_{i}$ 为截距项，$e_{it}$ 和 $\varepsilon_{it}$ 为误差项。在模型中，$\beta_{i}$ 被设定为异质系数，随机分布在一个平均值周围，即 $\beta_{i}=\beta+v_{i},\,v_{i} \sim IID(0,\Omega_{v})$ (Pesaran and Smith, 1995) 。

式 (1) 中，$f_{t}$ 表示周期性因素、中央政府政策等只随时间变化的不可观测变量；**式 (2) 表明不可观测因子 $\mathbf{f_{t}}$ 与自变量 $\mathbf{x_{t}}$ 相关，这一特征为相关研究所强调** (Pesaran, 2006) 。

为了获得对于 $\beta_{i}$ 的一致估计，需要克服不可观测因子 $f_{t}$ 所带来的影响。Pesaran (2006) 证明，**在 $\mathbf{x_{i,t}}$ 严格外生的条件下，利用可观测变量 (解释变量和被解释变量) 的截面均值作为不可观测因子的代理变量近似未观察到的共同因子，可以获得对于 $\mathbf{\beta_{i}}$ 的一致估计**。这种估计量通常被称为共同相关效应估计量。

下面简要说明 Pesaran (2006) 的基本思路：

记

$$
z_{it}=\begin{pmatrix}y_{it}\\x_{it}\end{pmatrix}=C_{i}^{T}f_{t}+u_{it}\quad(3)
$$

其中，$u_{it}=\begin{pmatrix}\alpha_{i}+e_{it}+\beta^{T}\varepsilon_{it}\\\varepsilon_{it}\end{pmatrix}$，$C_{i}=\tilde{\Gamma}_{i}\begin{pmatrix}1&0\\\beta_{i}&I_{k}\end{pmatrix}$，$\tilde{\Gamma}_{i}=\begin{pmatrix}\gamma_{i}&\Gamma_{i}\end{pmatrix}$，$I_{k}$ 为 $k$ 阶单位矩阵。

对式 (3) 取截面内平均，得到：

$$
\bar{z}_{t}=\bar{C}^{T}f_{t}+\bar{u}_{t}\quad(4)
$$

其中，$\bar{z}_{t}=\frac{1}{N}{\textstyle\sum_{i=1}^{N}}z_{it}$，$\bar{C}$，$\bar{u}_{t}$ 类似。

若 $\bar{C}$ 行满秩，根据式 (4)，可以得到：

$$
f_{t}=(\bar{C}\bar{C}^{T})^{-1}\bar{C}(\bar{z}_{t}-\bar{u}_t)\quad(5)
$$

Pesaran 和 Tosetti (2007) 证明当 $(N,T)\Rightarrow\infty$ 时，$\bar{u}_{t}\overset{q.m.}{\rightarrow}0$ ，即 $\bar{u}_{t}$ 依均方收敛于0。因此根据式 (5) 得到，当 $N\longrightarrow\infty$ 时，$f_{t}-(\bar{C}\bar{C}^{T})^{-1}\bar{C}(\bar{z}_{t}-\bar{u}_t)\overset{p}{\rightarrow}0$。它表明可以使用 $\bar{z}_{t}^{T}$ 作为 $f_{t}$ 的代理变量。

根据上述推导，我们可以得出**共同相关效应估计量的推导方法**，即**将自变量和因变量的截面均值作为共同因子的代理变量代入方程**，便可得到常见的面板模型形式。最后的估计方程可以表示如下：

$$
y_{it}=\alpha_{i}+\beta_{i}^{T}x_{it}+\delta_{1i}\bar{y}_{t}+\delta_{2i}\bar{x}_{t}+e_{it}\quad(6)
$$

其中的 $\delta_{1i}\bar{y}_{t}+\delta_{2i}\bar{x}_{t}$ 为 $\gamma_{i}^{T}f_{t}$ 的代理变量。

### 2.4 动态共同相关效应 (Dynamic Common Correlated Effects)

然而，**共同相关效应估计值仅在非动态面板中一致** (Chudik 和 Pesaran, 2013；Everart 和 De Groote, 2016) 。在动态面板中，模型设定如下：

$$
y_{it}=\alpha_{i}+\lambda_{i}y_{i,t-1}+\beta_{i}^{T}x_{it}+\gamma_{i}^{T}f_{t}+e_{it}\quad(7)
$$

$$
x_{it}=A_{i}^{T}d_{t}+\Gamma_{i}^{T}f_{t}+\varepsilon_{it}\quad(8)
$$

**滞后的因变量不再是严格的外生变量，因此估计量变得不一致**。Chudik 和 Pesaran (2013) 证明，如果将横截面平均值的 $\sqrt[3]{T}$ 期滞后项作为 $\gamma_{i}^{T}f_{t}$ 的代理变量，$\beta_{i}$ 估计量会具有一致性。估计方程变为：

$$
y_{i,t}=\alpha_{i}+\lambda_{i}y_{i,t-1}+\beta_{i}x_{i,t}+\sum_{l=0}^{p_{T}}\delta_{i,l}\bar{z}_{t-l}+e_{i,t}\quad(9)
$$

式中，$\bar{z}_{t}=(\bar{y}_{t-1},\bar{x}_{t})$ ， $p_{T}$ 是滞后期数 $\sqrt[3]{T}$。

根据 Chudik 和 Pesaran (2013) ，令 $\pi_{i}=(\lambda_{i},\beta_{i})$ 。其平均值组估计为

$$
\hat{\pi}_{MG}=\frac{1}{N}\sum_{i=1}^{N}\hat{\pi}_{i}\quad(10)
$$

如果 $(N,T,p_{T})\Rightarrow\infty$，则 $\hat{\pi}_{MG}$ 的一致估计以 $\sqrt{N}$ 的速度收敛。并且，在因子载荷满秩的情况下，渐近方差可以一致估计为，平均值组估计 $\hat{\pi}_{MG}$ 具有以下渐近分布：

$$
\sqrt{N}(\hat{\pi}_{MG})\,{\overset{d}{\rightarrow}}\,N(0,\sum{}^{}_{MG})
$$

综上，我们可以得到，**为了获得动态共同相关效应的一致估计**，我们只需要**如式 (9), 将 $\mathbf{(\bar{y}_{t-1},\bar{x}_{t})}$ 的 $\mathbf{\sqrt[3]{T}}$ 期滞后项作为共同因子的代理变量代入方程**，之后便可运用传统的动态面板数据处理方法获得对于参数项的一致估计。

&emsp;

## 3. xtdcce2 命令介绍

### 3.1 基本介绍

本部分将介绍由 Jan Ditzen (2018) 编写的新的 Stata 命令 `xtdcce2`，用于估计动态共同相关效应模型。本命令进行动态共同相关效应估计的方法主要遵循 Chudik 和 Pesaran (2013)，此外还支持共同相关效应估计 (Pesaran, 2006)。

### 3.2 安装方法

`xtdcce2` 是外部命令，可以使用如下命令安装最新版本：

```stata
ssc install xtdcce2, replace
```

**需要特别强调的是，在使用 `xtdcce2` 的过程中会调用外部命令 `moremata`** 。因此，需要在命令窗口输入以下命令安装程序文件 `moremata`：

```stata
ssc install moremata, replace
```

如果无法在 Stata 内部安装 `moredata`，可参考连享会码云文章 [moremata_install](https://gitee.com/arlionn/moremata_install) 中的方法。

### 3.3 语法结构

`xtdcce2`命令基本语法结构如下：

```stata
xtdcce2 depvar [indepvars] [varlist2 = varlist_iv] [if] [in] ,    ///
        crosssectional(varlist) [ pooled(varlist) cr_lags(string) ///
        nocrosssectional ivreg2options(string) e_ivreg2 ivslow    ///
        noisily lr(varlist) lr_options(string) pooledconstant     ///
        reportconstant noconstant trend pooledtrend               ///
        jackknife recursive exponent nocd showindividual          ///
        fullsample fast blockdiaguse nodimcheck useqr             ///
        useinvsym showomitted NOOMITted] 
```

- `depvar`：必选项，为因变量；

- `indepvars`：可选项，为自变量；

- `crosssectional(varlist)`：定义作为横截面平均值添加到公式中的变量；

- `pooled(varlist)`：选定均匀系数；

- `exponent`：进行截面相关检验；

- `cr_lags(integers)`：制定横截面平均值的滞后数；

- `nocrosssectional`：防止添加横截面平均数；

- `reportconstant`：报告常数项，如果未指定，则将常数项作为截面平均值的一部分。

&emsp;

## 4. Stata 实例

为了演示 `xtdcce2` 的用法，我们借助作者 Jan Ditzen (2018) 提供的数据，估计具有动态共同相关效应的扩展的索罗模型 (Augmented Solow Model) 。

### 4.1 数据结构

Barro 等 (1992) , Islam (1995) 和 Lee 等 (1997) 采用扩展的索罗增长模型，将 GDP 的差分项对每单位资本对应的 GDP 的滞后项、人力资本、物质资本和人口增长率进行回归。

我们使用的数据来自佩恩表 (PWT) , 它是由美国宾夕法尼亚大学生产/收入/价格国际比较研究中心编制的 188 个国家的购买力平价 GDP 比较数据。具体数据可以从下述网站中获取：

```stata
*- 数据存放地址：
*- https://gitee.com/arlionn/data/blob/master/data01/aging_growth_data.dta
*- Note: 进入网页后，请不要直接点击下载，而是右击【下载(95.49 KB)】→【链接另存为……】。
```

我们使用的数据包含 93 个国家 (N=93) 从 1960 年到 2007 年 (T=48) 的年度数据。数据共包括六个变量，其中 **id** 和 **year** 分别表示样本国家的代码 (截面变量) 和观察的年份 (时间变量)；**log_rgdpo** 表示实际 GDP ；**log_hc** 表示人力资本；**log_ck** 表示物质资本；**log_ngd** 表示人口增长率。

```stata
· use xtdcce2data.dta
. sum

    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
          id |      4,464          47    26.84824          1         93
        year |      4,464      1983.5    13.85495       1960       2007
   log_rgdpo |      4,464    8.417596     1.22088    5.09353   10.99178
      log_hc |      4,464    .6768531    .3095766   .0179907   1.278799
      log_ck |      4,464    11.21916    2.243923   6.266755   17.54079
     log_ngd |      4,371   -2.690039    .1669866  -4.210328  -1.696337
```

`log_rgdpo`：实际 GDP

`log_hc`：人力资本

`log_ck`：物质资本

`log_ngd`：人口增长率

在开始估计前需要先设置面板数据：

```stata
. xtset id year
       panel variable:  id (strongly balanced)
        time variable:  year, 1960 to 2007
                delta:  1 unit
```

### 4.2 共同相关效应估计

我们首先演示如何使用 `xtdcce2` 进行共同相关效应估计。索罗模型的共同相关效应估计可由以下方程的回归得到：

$$
d.log\_rgdpo_{it}=\alpha_{i}+\delta_{1i}\bar{y}_{t}+\delta_{2i}\bar{x}_{t}+\beta_{1}log\_hc_{it}+\beta_{2}log\_ck_{it}+\beta_{3}log\_ngd_{it}+u_{it}\quad(11)
$$

为了得到 $\beta_{i}$ 的一致估计，我们通过 `crossectional()` 将因变量和三个自变量作为截面平均值，并通过以下 Stata 命令获得 $\beta_{i}$ 的一致估计量：

```stata
. xtdcce2 d.log_rgdpo log_hc log_ck log_ngd , reportconstant crossectional(d.log_rgdpo log_hc log_ck log_ngd)

(Dynamic) Common Correlated Effects Estimator - Mean Group

Panel Variable (i): id                           Number of obs     =       4371
Time Variable (t): year                          Number of groups  =         93

Degrees of freedom per group:                    Obs per group (T) =         47
 without cross-sectional averages   = 43
 with cross-sectional averages      = 39
Number of                                        F(744, 3627)      =       0.51
 cross-sectional lags               = 0          Prob > F          =       1.00
 variables in mean group regression = 372        R-squared         =       0.91
 variables partialled out           = 372        R-squared (MG)    =       0.09
                                                 Root MSE          =       0.08
                                                 CD Statistic      =       2.36
                                                    p-value        =     0.0182
-------------------------------------------------------------------------------
    D.log_rgdpo|     Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
---------------+---------------------------------------------------------------
 Mean Group:   |
         log_hc|   .036763   .1386522    0.27    0.791     -.2349903   .3085162
         log_ck| -.0360335   .0175865   -2.05    0.040     -.0705025  -.0015645
        log_ngd| -.0994601   .0604876   -1.64    0.100     -.2180137   .0190934
          _cons| -.4849369   .5133691   -0.94    0.345     -1.491122    .521248
-------------------------------------------------------------------------------
Mean Group Variables: log_hc log_ck log_ngd _cons
Cross-sectional Averaged Variables: D.log_rgdpo log_hc log_ck log_ngd
```

此外，**在我们进行共同相关效应估计时，还需要验证面板数据是否是截面相关的，因此我们需要进行截面相关检验**。在 `xtdcce2` 的估计命令中，已经自动包含了用于检验截面相关的 CD 检验，并在输出结果中进行展示。

**CD 检验的原假设是 “截面之间相互独立”**，用数学形式表达即为：

$$
H_{0}:E(u_{i,t}u_{j,t})=0,\forall \,\, t \,\, and \,\, i \ne j
$$

其中的 $u_{i,t}$ 和 $u_{j,t}$ 即为式 (11) 中的误差项。

**CD 检验统计量**则为：

$$
CD=\sqrt{\frac{2T}{N(N-1)}}(\sum_{i=1}^{N-1}\sum_{j=i+1}^{N-1}\hat{\rho}_{ij})
$$

$$
\rho_{ij}=\rho_{ji}=\frac{{\textstyle\sum_{_{t-1}}^{T}\hat{u}_{i,t}\hat{u}_{j,t}}}{(\textstyle\sum_{_{t-1}}^{T}\hat{u}_{i,t})^{1/2}(\textstyle\sum_{_{t-1}}^{T}\hat{u}_{j,t})^{1/2}}
$$

从估计结果中我们可以看到，在上述共同相关效应中，CD 检验统计量为 2.36 ，p 值为 0.0182 。 这说明，我们可以拒绝原假设，截面之间是相关的。

### 4.3 动态共同相关效应估计

接下来，我们进行动态共同相关效应估计，通过 `crossectional()` 将因变量和四个自变量作为截面平均值截面平均值。此外，我们还需要通过 `cr_lags()` 设定横截面平均值的滞后数。

实际应用中，我们根据面板数据的时间长度取 $\sqrt[3]{T}$ 作为横截面平均值的滞后数。在本小节的例子中，我们令横截面平均值的滞后数为 3 。

扩展索罗模型的动态共同相关效应下可由以下方程回归得到：

$$
y_{it}=\alpha_{i}+\lambda_{i}y_{i,t-l}+\sum_{l=0}^{3}\delta_{i,l}\bar{y}_{t-1-l}+\sum_{l=0}^{3}\delta_{i,l}\bar{x}_{t-l}+\beta_{1}log\_hc_{it}+\beta_{2}log\_ck_{it}+\beta_{3}log\_ngd_{it}+u_{it}\quad(12)
$$

```stata
. xtdcce2 d.log_rgdpo L.log_rgdpo log_hc log_ck log_ngd , reportconstant crossectional(d.log_rgdpo L.log_rgdpo  log_hc log_ck log_ngd) cr_lags(3)

(Dynamic) Common Correlated Effects Estimator - Mean Group

Panel Variable (i): id                           Number of obs     =       4092
Time Variable (t): year                          Number of groups  =         93

Degrees of freedom per group:                    Obs per group (T) =         44
 without cross-sectional averages   = 39
 with cross-sectional averages      = 19
Number of                                        F(2325, 1767)     =       0.81
 cross-sectional lags               = 3          Prob > F          =       1.00
 variables in mean group regression = 465        R-squared         =       0.48
 variables partialled out           = 1860       R-squared (MG)    =       0.44
                                                 Root MSE          =       0.06
                                                 CD Statistic      =       1.37
                                                    p-value        =     0.1716
-------------------------------------------------------------------------------
    D.log_rgdpo|     Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
---------------+---------------------------------------------------------------
 Mean Group:   |
    L.log_rgdpo| -.6366608   .0303968  -20.94    0.000     -.6962375  -.5770841
         log_hc| -1.308897   .3965706   -3.30    0.001     -2.086161  -.5316326
         log_ck|  .2209471    .051488    4.29    0.000      .1200325   .3218616
        log_ngd|  .0412863   .1055255    0.39    0.696     -.1655399   .2481125
          _cons| -1.873393   1.834748   -1.02    0.307     -5.469432   1.722646
-------------------------------------------------------------------------------
Mean Group Variables: L.log_rgdpo log_hc log_ck log_ngd _cons
Cross-sectional Averaged Variables: D.log_rgdpo L.log_rgdpo log_hc log_ck log_ngd
```

在这里，CD 检验统计量等于 1.37 ，p 值为 0.1716 ，我们不能拒绝截面独立的原假设。

## 5.总结

在推文最后，我们回顾一下文章的主要内容：我们简单了解了什么是截面相关、截面相关的表现有什么，介绍了截面相关中的共同因子模型及其对应的共同相关效应及动态共同相关效应模型。我们可以看到，随着时代的发展，可用来研究的经济数据越来越多，对于大 N 大 T 型面板数据模型的研究日渐成为主流，动态共同相关效应模型施展拳脚的空间也将越来越大。

## 6. 参考文献

### 6.1 主要参考文献及 PDF 链接

 - **共同相关效应**：Pesaran M H. Estimation and Inference in Large Heterogeneous Panels with a Multifactor Error Structure[J]. Econometrica, 2006, 74(4):967-1012. [-PDF-](https://www.sci-hub.ren/10.2307/3805914)
- **动态共同相关效应**：Chudik A, Pesaran M H. Common correlated effects estimation of heterogeneous dynamic panel data models with weakly exogenous regressors[J]. Journal of Econometrics, 2015, 188(2): 393-420. [-PDF-](https://www.sci-hub.ren/10.1016/j.jeconom.2015.03.007)
- **截面相关检验 — CD 检验**：Pesaran M H. Testing Weak Cross-Sectional Dependence in Large Panels[J]. Econometric Reviews, 2015, 34(6-10): 1089–1117. [-PDF-](https://www.sci-hub.ren/10.1080/07474938.2014.956623)
- **截面相关检验 — BPK 检验**：Bailey N G, Kapetanios M H, Pesaran M H. Exponent of cross-sectional dependence: estimation and inference[J]. Journal of Applied Econometrics, 2016, 31: 929-960. [-PDF-](https://www.sci-hub.ren/10.1002/jae.2476)
- **Stata 命令 `xtdcce2`**：Jan Ditzen. Estimating Dynamic Common-Correlated Effects in Stata[J]. Stata Journal, 2018, 18(3): 585–617. [-PDF-](https://www.sci-hub.ren/10.1177/1536867x1801800306)
- **学习指南**：如果想要更加深入学习动态共同相关效应的具体用法，一方面可以阅读上述所列的论文，另一方面可以查看 [xtdcce2 应用文献](https://academic.microsoft.com/paper/2508039176/citedby/search?q=xtdcce%3A%20Estimating%20Dynamic%20Common%20Correlated%20Effects%20in%20Stata&qe=RId%253D2508039176&f=&orderBy=0)。此外，`xtdcce2` 命令语法和功能处于持续更新状态中，如果要使用 `xtdcce2` , 要认真查阅最新版的 help 文档。

### 6.2 全部参考文献

- Ahn S C, Lee Y H, Schmidt P. GMM estimation of linear panel data models with time-varying individual effects[J]. Journal of Econometrics, 2001, 101(2):219-255.
- Alexander, Chudik M. Weak and strong cross-section dependence and estimation of large panels[J]. The Econometrics Journal, 2011, 14(1):C45-C90.
- Bai J, Ng S. Determining the Number of Factors in Approximate Factor Models[J]. Econometrica, 2002, 70(1):191-221.
- Bai J. Panel Data Models With Interactive Fixed Effects[J]. Econometrica, 2009, 77(4):1229-1279.
- Bailey N G, Kapetanios M H, Pesaran M H. Exponent of cross-sectional dependence: estimation and inference[J]. Journal of Applied Econometrics, 2016, 31: 929-960.
- Barro R J, Mankiw N G, Sala-I-Martin X. Capital Mobility in Neoclassical Models of Growth[J]. NBER Working Papers, 1992.
- Breitung, Jörg, Pesaran M H. Unit Roots and Cointegration in Panels[J]. Discussion Paper Series 1: Economic Studies, 2008.
- Cahan E. Inferential Theory for Factor Models of Large Dimensions under Monotone Missing Data[D]. 2013.
- Chudik A, Pesaran M H. Large Panel Data Models with Cross-Sectional Dependence: A Survey[J]. Globalization & Monetary Policy Institute Working Paper, 2013.
- Chudik A, Pesaran M H. Common correlated effects estimation of heterogeneous dynamic panel data models with weakly exogenous regressors[J]. Journal of Econometrics, 2015, 188(2): 393-420.
- Coakley J, Fuertes A M, Smith R. A Principal Components Approach to Cross-Section Dependence in Panels[C]. International Conferences on Panel Data, 2002.
- Gerdie, Everaert, Tom. Common Correlated Effects Estimation of Dynamic Panels with Cross-Sectional Dependence[J]. Econometric Reviews, 2016.
- Han C, Orea L, Schmidt P. Estimation of a panel data model with parametric temporal variation in individual effects[J]. Journal of Econometrics, 2005, 126(2):241-267.
- Jan Ditzen. Estimating Dynamic Common-Correlated Effects in Stata[J]. Stata Journal, 2018, 18(3): 585–617.
- Lee K, Smith R. Growth and convergence in a multi-country empirical stochastic Solow model[J]. Journal of Applied Econometrics, 1997.
- Lee Y H. A stochastic production frontier model with group-specific temporal variation in technical efficiency[J]. European Journal of Operational Research, 2006, 174(3):1616-1630.
- Nazrul I. Growth Empirics: A Panel Data Approach[J]. The Quarterly Journal of Economics, 1998(1):1.
- Pesaran M H, Smith R. Estimating long-run relationships from dynamic heterogeneous panels[J]. Journal of Econometrics, 1995, 68.
- Pesaran M H, Tosetti E. Large Panels with Common Factors and Spatial Correlations[J]. Cambridge Working Papers in Economics, 2007, 161(2):182-202.
- Pesaran M H. Estimation and Inference in Large Heterogeneous Panels with a Multifactor Error Structure[J]. Econometrica, 2006, 74(4):967-1012.
- Pesaran, M H. Testing Weak Cross-Sectional Dependence in Large Panels.[J]. Econometric Reviews, 2015, 34(6-10): 1089-1117.
- Peter C. B. Phillips,Donggyu Sul. Dynamic panel estimation and homogeneity testing under cross section dependence[J]. The Econometrics Journal,2003,6(1).
- Sarafidis V, Wansbeek T. Cross-Sectional Dependence in Panel Data Analysis[J]. MPRA Paper, 2010, 31(5):483-531.
- 钱金保. 面板空间相关模型研究[M]. 科学出版社, 2013.
- 徐秋华, 张梓玚. 具有截面相关的变系数面板数据模型的估计及应用[J]. 系统工程理论与实践, 2019, 39(004):817-828.
- 连玉君，连享会码云文章，[moremata_install](https://gitee.com/arlionn/moremata_install).

## 7. 相关推文

> Note：产生如下推文列表的命令为：
> &emsp; `lianxh 面板数据, m`  
> 安装最新版 `lianxh` 命令：    
> &emsp; `ssc install lianxh, replace`

- 专题：[专题课程](https://www.lianxh.cn/blogs/44.html)
  - [⏩直播：动态面板数据模型](https://www.lianxh.cn/news/594aa12c096ca.html)
- 专题：[Stata教程](https://www.lianxh.cn/blogs/17.html)
  - [Stata：动态面板数据模型OLS估计的偏差](https://www.lianxh.cn/news/dba7c3067ae0a.html)
- 专题：[Stata命令](https://www.lianxh.cn/blogs/43.html)
  - [Stata新命令-tobalance：非平行面板转换为平行面板数据](https://www.lianxh.cn/news/f13e788da74a8.html)
- 专题：[数据处理](https://www.lianxh.cn/blogs/25.html)
  - [Stata数据处理：面板数据的填充和补漏](https://www.lianxh.cn/news/c2febe0f3530a.html)
- 专题：[面板数据](https://www.lianxh.cn/blogs/20.html)
  - [xtdpdgmm：动态面板数据模型一网打尽](https://www.lianxh.cn/news/b6713ec0cfa7a.html)
  - [Stata实操陷阱：动态面板数据模型](https://www.lianxh.cn/news/cc6c5ea80d70c.html)
  - [Stata面板：suest支持面板数据的似无相关检验](https://www.lianxh.cn/news/6a1b1959940fb.html)
  - [Stata新命令-tobalance：非平行面板转换为平行面板数据](https://www.lianxh.cn/news/c8772099446dd.html)
  - [Stata: 面板数据模型一文读懂](https://www.lianxh.cn/news/bf27906144b4e.html)
- 专题：[空间计量](https://www.lianxh.cn/blogs/29.html)
  - [空间面板数据模型及Stata实现](https://www.lianxh.cn/news/3567e7f105114.html)