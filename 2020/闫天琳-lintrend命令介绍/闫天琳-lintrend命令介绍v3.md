# lintrend命令介绍

&emsp;

> **作者**：闫天琳 (中山大学)    
> **E-Mail:** <yantlin@mail2.sysu.edu.cn>  
&emsp;

> **编者按**：本文部分摘译自下文，特此致谢！    
> **Source:** Garrett, J. M. 1996. sg50: Graphical assessment of linear trend. Stata Technical Bulletin 30: 9–15. In Stata Technical Bulletin Reprints, vol. 5, 152–160. College Station, TX: Stata Press.[-PDF-](https://www.stata-press.com/journals/stbcontents/stb30.pdf)

[toc]

## 1. 简介

本篇推文介绍 Stata 中用于线性趋势检验的命令：`lintrend`。该命令针对连续结果的分组的平均值，或二进制结果的对数概率，检验序数或区间X变量的线性假设，以及线性趋势的检验（基于线性或逻辑回归）。

## 2. lintrend  语法介绍
### 2.1 基础语法介绍

`lintrend`的语法结构如下：
```stata
· lintrend yvar xvar [if exp] [in range], [groups(#) round(#) int] [graph proportion noline graph_options]
```

- `yvar`: 因变量。可以是二元（0，1）结果，导致按 xvar 类别计算比例和对数几率，也可以是连续结果，导致按 xvar 类别计算均值。

- `xvar`: 自变量。可以是区间或序数自变量。


### 2.2 option  介绍

`[groups(#) round(#) int]`
| 命令      | 介绍 |
| :--------- | :--: |
| groups(#)     |  将 xvar 分成样本大小相似的#个类别。连接点放入较低的类别中。因此样本大小可能会略有不同。用每组 xvar 的均值代表这一组|
| round(#)   |  将 xvar 四舍五入到最接近的＃|
| integer |  xvar 是整数，直接使用其原始值|

`plot()`

`plot`是一个可选的 option，可选的图像类型：
- `mean` - xvar 分组的均值图（连续 yvar）
- `prop` - xvar 分组的比例图（二进制 yvar）
- `log` - xvar 分组的对数概率（二进制 yvar）图
- `both` - 比例和对数概率类别的图形

`noline` – 不生成图像


### 2.3 下载及安装

```stata
ssc install lintrend
```

## 3. Stata  实操：3 个实例

### 3.1 hyperten：探究与高血压有关的因素

**(1)使用`groups(#)`分组：检验高血压与年龄的关系**

数据(hyperten.dta)来源于一项对 1784 名年龄在 24 岁到 51 岁之间的成年人的研究。

```stata
use hyperten.dta,clear
describe
```

结果如下图所示，产生 4 个变量，1784 个观察值：

```stata

Contains data from hyperten.dta
  obs:         1,784                          
 vars:             4                          6 Feb 1996 16:49
 size:        12,488                          
----------------------------------------------------------------------------------------
              storage   display    value
variable name   type    format     label      variable label
----------------------------------------------------------------------------------------
sbpavg          float   %9.0g                 Average Systolic Blood Pressure
age             byte    %8.0g                 Current Age
ses             byte    %8.0g      seslbl     Socioeconomic Status
hbp             byte    %8.0g      yesno      High Blood Pressure
----------------------------------------------------------------------------------------
Sorted by: 
```

下面将样本分成 10 个大小大致相等的组，计算每个年龄组中高血压（1=高血压，0=正常）的比例和对数几率，并要求提供比例图和对数几率图。

用`lintrend`进行线性趋势检验：
```stata
lintrend hbp age, groups(10) plot(both) xlab ylab
```

结果如下：

```stata
The proportion and log odds of hbp by categories of age
  
  (Note: 10 age categories of equal sample size;
     Uses mean age value for each category)

  +------------------------------------------------+
  |  age   min   max    d   total    hbp   logodds |
  |------------------------------------------------|
  | 26.0    24    27   24     246   0.10     -2.22 |
  | 28.5    28    29   27     166   0.16     -1.64 |
  | 30.6    30    31   26     169   0.15     -1.70 |
  | 32.4    32    33   34     179   0.19     -1.45 |
  | 34.5    34    35   43     151   0.28     -0.92 |
  |------------------------------------------------|
  | 36.5    36    37   44     164   0.27     -1.00 |
  | 39.0    38    40   70     220   0.32     -0.76 |
  | 41.9    41    43   66     152   0.43     -0.26 |
  | 45.5    44    47   86     200   0.43     -0.28 |
  | 49.0    48    51   65     137   0.47     -0.10 |
  +------------------------------------------------+
```
所得比率图和对数几率图：


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%97%AB%E5%A4%A9%E7%90%B3_lintrend%E5%91%BD%E4%BB%A4%E4%BB%8B%E7%BB%8D_Fig01.png)


红线绘制了高血压患者的比例与每个类别的平均年龄的关系，圆圈绘制了对数优势与每个类别的平均年龄的关系，用“最佳拟合”线性回归线。图像表明，年龄和高血压之间存在正线性关系。

**(2)使用`round(#)`分组：检验高血压与年龄的关系**

将年龄按五年的增量进行分组。同样可以同时生成比例图和对数概率图。

```stata
lintrend hbp age, round(5) plot(both) xlab ylab
```

结果如下：
```stata
The proportion and log odds of hbp by categories of age
  
  (Note: age in categories rounded to nearest 5)

  +------------------------------------------------+
  | age   min   max     d   total    hbp   logodds |
  |------------------------------------------------|
  |  25    24    27    24     246   0.10     -2.22 |
  |  30    28    32    72     436   0.17     -1.62 |
  |  35    33    37   102     391   0.26     -1.04 |
  |  40    38    42   117     323   0.36     -0.57 |
  |  45    43    47   105     249   0.42     -0.32 |
  |------------------------------------------------|
  |  50    48    51    65     137   0.47     -0.10 |
  +------------------------------------------------+
```

可以得到比例图和对数概率图：


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%97%AB%E5%A4%A9%E7%90%B3_lintrend%E5%91%BD%E4%BB%A4%E4%BB%8B%E7%BB%8D_Fig02.png)


年龄的四舍五入值作为组别中点，每个组别范围是 5 年。组别的最小可能年龄值（**age**=25）是 23，但是，由于样本中没有这个年龄段的人，所以实际的最小值（24）被用作最小值。对于的最大值也是如此 **age**=50（51而不是52）。对数几率图再次表明了一种线性趋势，尽管高血压在 40 岁之前增加得更快，但在 40 岁以上则变慢。

**(3)使用integer分组：检验高血压与社会地位之间的关系**

高血压数据集中的另一个变量是社会经济状况的测量(**ses**)。这是一个序数变量，分为三类：低社会经济地位（1）、中等社会经济地位（2）和高社会经济地位（3）。这个例子要求每一类 **ses** 和高血压的比例，以及对数概率图。
```stata
lintrend hbp ses, integer plot(log) xlab(1,2,3) ylab
```

结果如下：
```stata
The proportion and log odds of hbp by categories of ses
  
  (Note: ses in categories using original values)

  +-----------------------------------------+
  |      ses     d   total    hbp   logodds |
  |-----------------------------------------|
  |    1:Low   215     670   0.32     -0.75 |
  | 2:Middle   138     537   0.26     -1.06 |
  |   3:High   117     512   0.23     -1.22 |
  +-----------------------------------------+
```
对数概率图：


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%97%AB%E5%A4%A9%E7%90%B3_lintrend%E5%91%BD%E4%BB%A4%E4%BB%8B%E7%BB%8D_Fig03.png)

从图像中，我们看到高血压随着社会经济地位的上升而减少，两者有较强的负线性相关性。


**(4)使用`groups(#)`分组：检验平均收缩压与年龄的关系**

前面的例子检验了连续的二元结果，`lintrend`会计算平均数而不计算比例和对数几率。假设本例中用平均收缩压(**sbpavg**)来代表高血压。我们将样本分成 10 组，每组大小大致相等，计算每个年龄组的平均收缩压，并绘制每个年龄组的平均收缩压与平均年龄的关系图。

```stata
lintrend sbpavg age, groups(10) plot(mean) xlab ylab
```

关系图如下：


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%97%AB%E5%A4%A9%E7%90%B3_lintrend%E5%91%BD%E4%BB%A4%E4%BB%8B%E7%BB%8D_Fig04.png)


年龄类别与(1)例中相同。收缩压随着年龄的增长而增加。这条线周围有波动，但本质上增长是线性的。

### 3.2 backpain：探究与背痛康复有关的特征

本例使用了来自背痛研究的数据（Carey 1995）。对 1552 例背痛患者进行了 6 个月的随访，以确定与康复相关的特征。这个例子研究了在基线水平上持续测量如何消除背痛之间的关系(**score**：可能值从0=无残疾到23=完全残疾）以及八周后的恢复概率(**better**：1=全部恢复，0=没有恢复）。残疾变量 **score** 被分为 12 类，样本大小相同。better 是二进制的，计算比例和对数几率。要求提供对数概率图。

```stata
use backpain.dta,clear
describe
```
结果如下图所示，产生 4 个变量，1552 个观察值：

```stata
Contains data from backpain.dta
  obs:         1,552                          Recovery from back pain
 vars:             4                          9 Feb 1996 16:32
 size:         9,312                          
----------------------------------------------------------------------------------------
              storage   display    value
variable name   type    format     label      variable label
----------------------------------------------------------------------------------------
better          byte    %8.0g      yesno      Better at 8 Weeks
score           byte    %8.0g                 Disability Score
score_2         int     %9.0g                 Disability Score Squared
score_3         int     %9.0g                 Disability Score Cubed
----------------------------------------------------------------------------------------
Sorted by: 

```

用`linrtrend`进行线性趋势检验：

```stata
lintrend better score, groups(12) plot(log) xlab ylab
```

结果如下：

```stata
The proportion and log odds of better by categories of score
  
  (Note: 12 score categories of equal sample size;
     Uses mean score value for each category)

  +----------------------------------------------------+
  | score   min   max     d   total   better   logodds |
  |----------------------------------------------------|
  |   0.4     0     1   151     170     0.89      2.07 |
  |   2.5     2     3   100     136     0.74      1.02 |
  |   4.6     4     5    69     105     0.66      0.65 |
  |   6.5     6     7    67     119     0.56      0.25 |
  |   8.5     8     9    75     139     0.54      0.16 |
  |----------------------------------------------------|
  |  10.5    10    11    60     128     0.47     -0.13 |
  |  12.5    12    13    57     118     0.48     -0.07 |
  |  15.1    14    16    96     197     0.49     -0.05 |
  |  17.0    17    17    33      79     0.42     -0.33 |
  |  18.5    18    19    49     147     0.33     -0.69 |
  |----------------------------------------------------|
  |  20.5    20    21    40     121     0.33     -0.71 |
  |  22.5    22    23    24      93     0.26     -1.06 |
  +----------------------------------------------------+

```

所得比例和对数概率图：


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%97%AB%E5%A4%A9%E7%90%B3_lintrend%E5%91%BD%E4%BB%A4%E4%BB%8B%E7%BB%8D_Fig05.png)



残疾评分较低的患者在 8 周前康复的可能性（最低级别 86%）远高于得分较高的患者（最高级别 24%），图像显示了这种下降趋势。然而，当残疾分数从 0 增加到 10 左右时，恢复速度会急剧下降，当残疾分数从 10 增加到 15 左右时，恢复速度又会趋于平稳，当残疾分数超过 15 时，恢复速度会继续急剧下降。这样的形状可能表明了非线性趋势，下面的逻辑回归模型，允许恢复的概率是残疾水平的三次函数，证实了图表所建议的关系。

```stata
logistic better score score_2 score_3
```

结果如下：
```stata

Logistic regression                             Number of obs     =      1,552
                                                LR chi2(3)        =     207.17
                                                Prob > chi2       =     0.0000
Log likelihood = -969.56991                     Pseudo R2         =     0.0965

------------------------------------------------------------------------------
      better | Odds Ratio   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
       score |      0.643      0.050    -5.71   0.000        0.553       0.748
     score_2 |      1.032      0.008     4.05   0.000        1.016       1.048
     score_3 |      0.999      0.000    -3.63   0.000        0.999       1.000
       _cons |      7.937      1.713     9.60   0.000        5.200      12.116
------------------------------------------------------------------------------
Note: _cons estimates baseline odds.

```

### 3.3 来自美国年轻妇女的研究数据的例子

下面使用美国年轻妇女的研究数据，分别检验几个变量的线性趋势关系：

```stata
sysuse "nlsw88.dta", clear
describe
```

结果如下图所示，产生 17 个变量，2246 个观察值：

```stata
Contains data from /Applications/Stata/ado/base/n/nlsw88.dta
  obs:         2,246                          NLSW, 1988 extract
 vars:            17                          1 May 2016 22:52
 size:        60,642                          (_dta has notes)
----------------------------------------------------------------------------------------
              storage   display    value
variable name   type    format     label      variable label
----------------------------------------------------------------------------------------
idcode          int     %8.0g                 NLS id
age             byte    %8.0g                 age in current year
race            byte    %8.0g      racelbl    race
married         byte    %8.0g      marlbl     married
never_married   byte    %8.0g                 never married
grade           byte    %8.0g                 current grade completed
collgrad        byte    %16.0g     gradlbl    college graduate
south           byte    %8.0g                 lives in south
smsa            byte    %9.0g      smsalbl    lives in SMSA
c_city          byte    %8.0g                 lives in central city
industry        byte    %23.0g     indlbl     industry
occupation      byte    %22.0g     occlbl     occupation
union           byte    %8.0g      unionlbl   union worker
wage            float   %9.0g                 hourly wage
hours           byte    %8.0g                 usual hours worked
ttl_exp         float   %9.0g                 total work experience
tenure          float   %9.0g                 job tenure (years)
----------------------------------------------------------------------------------------
Sorted by: idcode
```

**(1)探究工资与任期的关系：**

将样本分成十组，每组大小大致相等，计算每个任期组的平均工资，并绘制每个任期组的平均工资与平均任期的关系图。

```stata
lintrend wage tenure, groups(10) plot(mean) xlab ylab
```

结果如下：
```stata
The mean of wage by categories of tenure 
  
  (Note: 10 tenure categories of equal sample size;
     Uses mean tenure value for each category)

  +----------------------------------------------+
  | tenure        min         max   total   wage |
  |----------------------------------------------|
  |   0.24          0   .58333331     233   5.60 |
  |   1.01   .6666667   1.3333334     234   6.60 |
  |   1.66   1.416667   1.9166666     222   7.04 |
  |   2.47          2   2.9166667     212   7.91 |
  |   3.41          3   3.8333333     235   7.54 |
  |----------------------------------------------|
  |   4.72   3.916667   5.6666665     203   7.68 |
  |   6.83       5.75    8.166667     226   8.46 |
  |   9.38       8.25   10.583333     221   8.78 |
  |  12.60   10.66667       14.75     224   8.74 |
  |  17.90   14.83333   25.916666     221   9.75 |
  +----------------------------------------------+
```

可以得到图像：


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%97%AB%E5%A4%A9%E7%90%B3_lintrend%E5%91%BD%E4%BB%A4%E4%BB%8B%E7%BB%8D_Fig06.png)


工资随着任期的增长而增加。在任期 5 年之前，工资增长较快，5 年之后，增长速度放缓，但本质上增长是线性的。

**(2)探究工资与年龄的关系：**

将样本分成十组，每组大小大致相等，计算每个年龄组的平均工资，并绘制每个年龄组的平均工资与平均年龄的关系图。

```stata
lintrend wage age, groups(10) plot(mean) xlab ylab
```

结果如下：
```stata
The mean of wage by categories of age 
  
  (Note: 10 age categories of equal sample size;
     Uses mean age value for each category)

  +---------------------------------+
  |  age   min   max   total   wage |
  |---------------------------------|
  | 34.8    34    35     313   7.84 |
  | 36.0    36    36     257   8.14 |
  | 37.0    37    37     225   7.88 |
  | 38.0    38    38     219   8.12 |
  | 39.0    39    39     234   7.99 |
  |---------------------------------|
  | 40.0    40    40     208   7.68 |
  | 41.0    41    41     222   7.52 |
  | 42.0    42    42     160   7.33 |
  | 43.5    43    44     328   7.42 |
  | 45.0    45    46      80   7.54 |
  +---------------------------------+
```
关系图：


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%97%AB%E5%A4%A9%E7%90%B3_lintrend%E5%91%BD%E4%BB%A4%E4%BB%8B%E7%BB%8D_Fig07.png)


可以看到，工资在 40 岁前和 40 岁后的波动都较大，工资和年龄没有显著的线性关系。


**(3)探究工作经验与工资的关系：**

将样本分成八组，每组大小大致相等，计算每个工作经验组的平均工资，并要求绘制每个工作经验组的平均工资与平均工作经验的关系图。

```stata
lintrend wage ttl_exp, groups(8) plot(mean) xlab ylab
```
结果如下：

```stata
The mean of wage by categories of ttl_exp 
  
  (Note: 8 ttl_exp categories of equal sample size;
     Uses mean ttl_exp value for each category)

  +-----------------------------------------------+
  | ttl_exp        min         max   total   wage |
  |-----------------------------------------------|
  |     4.6   .1153846   6.6602564     281   4.89 |
  |     8.0   6.666667   9.2115383     281   5.66 |
  |    10.3   9.217949   11.346154     282   7.59 |
  |    12.2   11.35897   13.115385     279   7.75 |
  |    13.8   13.13462   14.519231     283   8.43 |
  |-----------------------------------------------|
  |    15.2   14.51923   15.974359     278   9.52 |
  |    16.8   15.98077       17.75     285   8.84 |
  |    19.4   17.76282   28.884615     277   9.48 |
  +-----------------------------------------------+
```
关系图如下：


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%97%AB%E5%A4%A9%E7%90%B3_lintrend%E5%91%BD%E4%BB%A4%E4%BB%8B%E7%BB%8D_Fig08.png)



红线绘制了平均工资与每个类别的平均工作经验的关系，用“最佳拟合”线性回归线。图像表明，工作经验和工资之间存在正线性关系。


**(4)探究工作经验与任期的关系：**

将样本分成八组，每组大小大致相等，计算每个任期组的平均工作经验，并要求绘制每个任期组的平均工作经验与平均任期的关系图。

```stata
lintrend ttl_exp tenure, groups(8) plot(mean) xlab ylab
```

结果如下：
```stata
The mean of ttl_exp by categories of tenure 
  
  (Note: 8 tenure categories of equal sample size;
     Uses mean tenure value for each category)

  +-------------------------------------------------+
  | tenure        min         max   total   ttl_exp |
  |-------------------------------------------------|
  |   0.34          0         .75     291      9.33 |
  |   1.24   .8333333   1.5833334     272     10.36 |
  |   2.09   1.666667   2.6666667     281     10.76 |
  |   3.30       2.75   3.8333333     292     11.91 |
  |   5.01   3.916667        6.25     264     12.07 |
  |-------------------------------------------------|
  |   7.80   6.333333    9.333333     276     13.60 |
  |  11.27   9.416667   13.666667     280     14.69 |
  |  17.16      13.75   25.916666     275     17.90 |
  +-------------------------------------------------+
```
关系图如下：


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E9%97%AB%E5%A4%A9%E7%90%B3_lintrend%E5%91%BD%E4%BB%A4%E4%BB%8B%E7%BB%8D_Fig09.png)



红线绘制了平均工作经验与每个类别的平均任期的关系，用“最佳拟合”线性回归线。图像表明，任期和工作经验之间存在正线性关系。



## 4. 结语
本篇推文主要使用 Stata 实操举例介绍了 Stata 用于检验线性趋势的命令 —— `lintrend` ，它能够根据变量特征，将其进行不同的分类，进而检验两个变量之间的线性趋势。其语法规则简单易懂，非常容易上手。同时可以提供几种不同的关系图像，直观显示出变量之间的相关关系，因此该命令有很高的实用价值。

## 参考文献
- Garrett, J. M. (2017). LINTREND: Stata module to graph observed proportions or means for a continuous or ordinal X variable.[-Link-](http://fmwww.bc.edu/repec/bocode/l/lintrend.ado)
- Garrett, J. M. 1996. sg50: Graphical assessment of linear trend. Stata Technical Bulletin 30: 9–15. In Stata Technical Bulletin Reprints, vol. 5, 152–160. College Station, TX: Stata Press.[-PDF-](https://www.stata-press.com/journals/stbcontents/stb30.pdf)














