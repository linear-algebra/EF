cd ""

//周老师的程序
timer clear 1
timer on 1
use data,clear
sum total_income
replace total_income=1 if total_income<1&total_income>0
count if total_income<1&total_income>0  //0

gen lnincome=log(total_income)
replace lnincome=0 if total_income==0

gen lnwealth=log(net_wealth)
replace lnwealth=0 if net_wealth==0

replace age=age/10
replace age2=age2/100   //let the coeff larger

global control lnwealth lnincome age age2 edu gender marriage risk_lover risk_averter size job rural east west 

probit inf1 iphone onlineshop cell we_fee $control
predict inf1hat,xb
gen inf=normal(inf1hat) //continuous variable

********
gen y=1 if rate_r>0 & rate_r!=.
replace y=0 if rate_r==0  
********

gen x1=lnsocial     //potential endogeneity
gen x2=inf
gen x1x2=x1*x2     //interaction term

gen iv1=iphone  //x1的IV
gen iv2=onlineshop
gen iv3=cell
gen iv4=we_fee

global control lnwealth lnincome age age2 edu gender marriage risk_lover risk_averter size job rural east west 
global ivs4 iv1 iv2 iv3 iv4 //four IVs

**first-stage:
cap drop v1hat
qui reg x1 $ivs4 x2 $control 
predict v1hat,resid
predict x1hat

***second-stage:
qui probit y x1 x2 x1x2 $control v1hat
predict yhat, xb
gen v=_b[v1]*v1hat
gen a1_b=yhat-v     //a1*x1+a2*x2+a3*x1*x2+x'b

drop if y==. | x1==. | x2==.
drop if a1_b==. | v==.

gen ave1=0
gen ave2=0
gen ave3=0

/*
gen id=_n    //对观察值编号，以与ave1_6数据.dta合并
merge id using "ave1_6数据.dta",sort
drop _merge
*/



local nn=_N
forvalues i=1/`nn'{
qui replace ave1=ave1+normal(a1_b+v[`i'])
qui replace ave2=ave2+normalden(a1_b+v[`i'])
qui replace ave3=ave3+(a1_b+v[`i'])*normalden(a1_b+v[`i'])
}
replace ave1=ave1/_N //average probability as a function of x1,x2,x
replace ave2=ave2/_N
replace ave3=ave3/_N   //数据保存于ave1_6数据.dta

gen mex1=(_b[x1]+_b[x1x2]*x2)*ave2
gen mex2=(_b[x2]+_b[x1x2]*x1)*ave2
gen inteff=_b[x1x2]*ave2-(_b[x2]+_b[x1x2]*x1)*(_b[x1]+_b[x1x2]*x2)*ave3  //interaction effect
gen meage=(_b[age]+2*_b[age2]*age)*ave2        //marginal propability effect of age
gen qeage=2*_b[age2]*ave2-(_b[age]+2*_b[age2]*age)^2*ave3    //quadratic effect of age

**calculate the average effects:mex1_m mex2_m inteff_m meage_m qeage_m
qui{
sum mex1 
scalar mex1_m=r(mean)    //mean marginal effect of x1
sum mex2 
scalar mex2_m=r(mean)      //mean marginal effect of x2
sum inteff 
scalar inteff_m=r(mean)    //mean interaction effects of x1,x2
sum meage 
scalar meage_m=r(mean)   //mean marginal effect of age
sum qeage 
scalar qeage_m=r(mean)   //mean quadratic effect of age
}

sum mex1 mex2 inteff meage qeage


***parametric bootstrap by residuals based on the two-step estimation

gen mex1_b=0
gen mex2_b=0
gen inteff_b=0
gen meage_b=0
gen qeage_b=0

set seed 10101
local B=100    //bootstrap replicates
local nn=_N  //sample size
forvalues i=1(1)`B'{
dis "run=" `i'
***first-stage
   cap drop v1hat x1hat yhat a1_b 
   qui reg x1 $ivs4 x2 $control 
   predict v1hat,resid
   predict x1hat,xb
***second-stage:
   qui probit y x1 x2 x1x2 $control v1hat
   predict yhat, xb
   gen a1_b=yhat-_b[v1hat]*v1hat    //a1*x1+a2*x2+a3*x1*x2+x'b

gen v1s=v1hat[ceil(uniform()*`nn')]  //resample the residuals v1f with replacement
gen x1s=x1hat+v1s         //generate the boostrap series of x1
gen x1sx2=x1s*x2
gen ave1s=0
 forvalues j=1(1)`nn'{
 qui replace ave1s=ave1s+normal(_b[x1]*x1s+_b[x2]*x2+_b[x1x2]*x1sx2+(a1_b-(_b[x1]*x1+_b[x2]*x2+_b[x1x2]*x1x2))+_b[v1hat]*v1s[`j'])
 }
 qui replace ave1s=ave1s/`nn'  //probability as a function of x1s,x2,x in observations

gen U=uniform()
qui gen ys=1 if U<=ave1s
qui replace ys=0 if U>ave1s     //generate bootstrap y*

drop U ave1s v1s 

***use boostrap sample {ys,x1s,x2,x1sx2,x,$ivs4} to estimate 
***first-stage:
  qui reg x1s $ivs4 x2 $control 
  predict v1ss,resid
***second-stage:
  qui probit ys x1s x2 x1sx2 $control v1ss
  predict xbs, xb
  gen vs=_b[v1ss]*v1ss
  gen a1_bs=xbs-vs   //a1*x1+a2*x2+a3*x1*x2+x'b

gen ave2s=0
gen ave3s=0


forvalues ii=1(1)`nn'{
qui replace ave2s=ave2s+normalden(a1_bs+vs[`ii'])
qui replace ave3s=ave3s+(a1_bs+vs[`ii'])*normalden(a1_bs+vs[`ii'])
}
qui replace ave2s=ave2s/`nn'
qui replace ave3s=ave3s/`nn'


gen mex1s=(_b[x1s]+_b[x1sx2]*x2)*ave2s              //marginal effects of x1 in observations
gen mex2s=(_b[x2]+_b[x1sx2]*x1s)*ave2s            //marginal effects of x2 in observations
gen inteffs=_b[x1sx2]*ave2s-(_b[x2]+_b[x1sx2]*x1s)*(_b[x1s]+_b[x1sx2]*x2)*ave3s   //interaction effects of x1,x2 in observations
gen meages=(_b[age]+2*_b[age2]*age)*ave2s               //marginal effects of age in observations
gen qeages=2*_b[age2]*ave2s-(_b[age]+2*_b[age2]*age)^2*ave3s   //quadratic effects of age in observations	
qui{
sum mex1s
replace mex1_b=r(mean) in `i'
sum mex2s
replace mex2_b=r(mean) in `i'
sum inteffs
replace inteff_b=r(mean) in `i'
sum meages
replace meage_b=r(mean)  in `i'
sum qeages
replace qeage_b=r(mean) in `i'
}
drop mex1s mex2s inteffs meages qeages ave2s ave3s a1_bs vs xbs v1ss ys x1sx2 x1s v1s
}

** now obtain the bootstraped sample of effects {mex1_b mex2_b inteff_b meage_b qeage_b, b=1,2,...,B}
drop if mex1_b==0
drop if mex2_b==0
drop if inteff_b==0
drop if meage_b==0
drop if qeage_b==0

**calculate bootstrapped se, z, pv, CI of mex1_m mex2_m inteff_m meage_m qeage_m
local effects mex1 mex2 inteff meage qeage
foreach A of local effects {
qui sum `A'_b
scalar `A'_bse=r(sd)  //bootstrap se
scalar `A'_bz=`A'_m/`A'_bse  //bootstrap z
scalar `A'_pv= 2*(1-normal(abs(`A'_bz)))   //bootstrap p-value based on normality
scalar `A'_l=`A'_m-1.96*`A'_bse
scalar `A'_r=`A'_m+1.96*`A'_bse  //bootstrapped CI based on normality
}


***display in a table
dis  "          mean        se           z           pv          confidence_interval"    _newline  ///
     " mex1:   " %6.4f mex1_m  "     "  %6.4f mex1_bse  "      "  %6.4f mex1_bz  "       " %6.4f mex1_pv "       " %6.4f mex1_l "       "   %6.4f mex1_r     _newline  ///
     " mex2:   " %6.4f mex2_m  "     "  %6.4f mex2_bse  "      "  %6.4f mex2_bz  "       " %6.4f mex2_pv "        " %6.4f mex2_l "       "   %6.4f mex2_r    _newline  ///	   
     " inteff: " %6.4f inteff_m  "     "  %6.4f inteff_bse  "      "  %6.4f inteff_bz  "       " %6.4f inteff_pv "       " %6.4f inteff_l "       "   %6.4f inteff_r        _newline  ///
     " meage:  " %6.4f meage_m  "     "  %6.4f meage_bse  "      "  %6.4f meage_bz  "       " %6.4f meage_pv  "        " %6.4f meage_l "       "   %6.4f meage_r  _newline  ///
     " qeage: " %6.4f qeage_m  "     "  %6.4f qeage_bse  "     "  %6.4f qeage_bz  "       " %6.4f qeage_pv "       " %6.4f qeage_l "      "   %6.4f qeage_r _newline 
timer off 1
//结束计时
timer list 1 





















//eivprobit
use data,clear


sum total_income
replace total_income=1 if total_income<1&total_income>0
count if total_income<1&total_income>0  //0

gen lnincome=log(total_income)
replace lnincome=0 if total_income==0

gen lnwealth=log(net_wealth)
replace lnwealth=0 if net_wealth==0

replace age=age/10
replace age2=age2/100   //let the coeff larger

global control lnwealth lnincome age age2 edu gender marriage risk_lover risk_averter size job rural east west 

probit inf1 iphone onlineshop cell we_fee $control
predict inf1hat,xb
gen inf=normal(inf1hat) //continuous variable

********
gen y=1 if rate_r>0 & rate_r!=.
replace y=0 if rate_r==0  
********

gen x1=lnsocial     //potential endogeneity
gen x2=inf
gen x1x2=x1*x2     //interaction term

gen iv1=iphone  //x1的IV
gen iv2=onlineshop
gen iv3=cell
gen iv4=we_fee

global control lnwealth lnincome age edu gender marriage risk_lover risk_averter size job rural east west 
global ivs4 iv1 iv2 iv3 iv4 //four IVs

timer clear 1
timer on 1
eivprobit y $control (x1 = iv1 iv2 iv3 iv4), interact(x2) quaeffect(age) bootstrap(100) seed(10101)
timer off 1
//结束计时
timer list 1