capture program drop myfact1
program define myfact1
version 15

  args k    // 定义暂元 k，用于接收用户输入的数字
  
  local a = 1
  forvalues i = 1/`k'{
    local a = `a'*`i'
  }
  
  dis "`k'! = " `a'
  
end  
