&#x1F449;  点击右上方的【编辑】按钮，可以查看 Markdown 原始文档。

> **作者：** 王文泽 (中山大学)  
> **邮箱：** wangwz5@sysu.edu.cn

&emsp;

# addest 命令介绍



------

### 目录

[TOC]

------



## **1.简介**

本文将介绍`addest`命令的基础内容和使用范例。` addest`命令以 Stata 中的常用命令`ereturn`作为基础， 在执行过`ereturn`命令并生成带有一组估计结果的存储项后，用户通过`addest`命令将一些选项添加到已有存储项中，丰富了使用`ereturn` 产生的估计结果可进行的后续命令操作，增加了`ereturn`命令的功能。



## 2.addest 内容介绍

### 2.1 addest 理论基础

`addest` ——在估计结果中添加标量、向量、矩阵或文本信息。

以`ereturn`命令为例，执行估计操作之后，每个估计命令都将其结果留在Stata内存中。 然后，用户可以通过输入`ereturn list`来查看这些已保存的结果，并在随后的计算过程里，通过引用存储在内存空间中的元素来获得各种估计结果。 例如，用户可以通过输入：`display e(N)`来获取最近使用的估计命令的样本大小。

此外，许多由 Stata 官方或用户提供的 post-estimation 命令都依赖于`ereturn`命令执行后的内容来进行进一步的分析， 例如 Stata 中功能强大的`margins`和`marginsplot`命令，以及相较普通的`estimates table`命令，都使用存储的估计结果，其中 `estimates table`命令因其能够并排显示几个估计模型的结果而十分常用。 同样类似且功能强大的几个输出表格的命令包括有`esttab`、`xml_tab`、`outreg`和`outreg2`等。

因此会有用户希望 post-estimation 命令在显示或操作时，保留原始估计模型的某些特性，而不是仅作为存储结果的一部分时出现，`addest`命令则解决了这一问题。

### 2.2 addest 语法介绍

`addest`基础语法结构如下：

```stata
 addest [ , name(charstring) value(numeric value) bvector(vector name) vcematrix(matrix name) rename augbvector(number)
                 augvcematrix(number) augcoefname(string) augeqname(string)
                 textname(string) textstring(string) matname(ematrix_name)
                 matrix(existing_matrix) findomitted buildfvinfo post repost *
                 ]

```

### 2.3 options 介绍

* `name(charstring)) value(numeric value)`

  这两个选项必须同时设定来进行指定。 `name`选项要添加到`ereturn`存储空间中标量的名称，因为它会成为 Stata 宏指令的名称，所以所指定名称的字符串中不能包含空格或特殊字符。如果用户指定已经存在的名称，则其值将被覆盖。

* `bvector(vector name) vceatrix(matrix name)`

  这两个选项也必须同时进行设定。 它们使用户能够替换估计系数的向量及其方差协方差矩阵。新的系数向量和方差协方差矩阵必须与目前在`ereturn`存储空间中的矩阵具有相同的行数和列数。

* `rename `

  这个选项只允许与 `bvector(vector name) vceatrix(matrix name)`语法一同进行使用，并使Stata使用从指定的b向量获得的名称，来作为 **b** 和 **V** 估计矩阵的标签， 这些标签随后会被用于由`ereturn display`或`estimates table`产生的输出结果。

* `augbvector(number) augvceatrix(number) augcoefname(string) [ augeqname(string) ]`

  这四个选项是同一个组合，其中前三个选项需要同时进行设定。 最后的第四个选项指定了添加系数估计的方程名称，只有当 **e(b)** 和 **e(V)** 结果已经存储在**e（return) **中时包括方程式名称。这些选项使用户能够将单个估计系数添加到系数向量 **e(b)** 中，并将其估计方差添加到的估计方差与协方差的系数矩阵 **e(V)** 中。

* `textname(string_with_no_blanks) textstring(string_with_no_blanks)`

  这两个选项必须同时进行设定的，它们可以用于在 **e(return)** 的结果中添加任意字符串作为宏指令。

* `matname(ematrix_name) matrix(existing_matrix)`

  这两个选项是必须同时进行设定。它们先将之前创建命名为 **existing_matrix** 的 Stata 矩阵加载进入`ereturn`的存储空间中，并更改为名为 **ematrix_name** 的矩阵，其中  **ematrix_name **中不能包含空格或特殊字符。 如果用户在设定使用了一个已经存在于`ereturn`空间中的名称，那个名称将被覆盖。

- `post` 和` repost `

  当用户更改或更新`ereturn`存储空间中的估计结果时，`addest`命令默认设定为`repost`选项。当用户更改或更新涉及到`ereturn`存储空间的其他内容时，`addest`命令默认设定为`post`选项。同时用户也可以通过直接输入`post`或`repost`指令来覆盖这些默认选项。

- `findomitted`

  通过`ereturn post`和`ereturn repost`来指定。将省略运算符`o.`添加到列表中 **e(V)** 的零值对角线元素对应的变量中，此选项的设定会同样传递给`ereturn repost`。

- `buildfvinfo`

  通过`ereturn post`和`ereturn repost`来指定，并计算用于测定可估计函数的 H 矩阵，该矩阵将会用于一些 post-estimation 命令，例如：`contrast`，`margins`和`pwcompare`。 此选项的设定会同样传递给`ereturn repost`

- `*(other options) `

   任何其他选项的设定都会被传递到`ereturn post`和`ereturn repost`。

## 3. Stata实操范例

### 3.1 命令安装

直接安装命令：

```stata
ssc install addest
```

或者搜索后安装：

```stata
findit addest
```

### 3.2 操作范例

在这个范例中，我们将使用 Galton 所作关于儿童身高的原始数据集，他曾经用这些数据来表明儿童的身高可以从父母的身高准确地预测，尽管最终结果并不是准确的。为了能调查身高与父母的关系，原始数据中已得到了关于儿童年龄的数据（实际上 Galton 只使用了已成年儿童的数据）。 这个包含儿童年龄的数据集称为galton.dta，可以通过 Stata 直接下载。

下载数据集：

```stata
use galton, clear
```

假设一个基于父亲的身高 fheight 、母亲的身高 mheight 和孩子的年龄来预测孩子的身高的模型，其中孩子的年龄变量是作为三个虚拟变量的集合，分别定义为：

> 如果年龄> 5，  Dage5  = 1， 否则 Dage5 = 0
>
> 如果年龄> 10，Dage10= 1，否则 Dage10 = 0
>
> 如果年龄> 15，Dage15= 1，否则 Dage15 = 0
>
> 如果年龄小于或等于5岁，则所有三个虚拟变量都为0

```stata
gen Dage5 = (age>5)
gen Dage10=(age>10)
gen Dage15=(age>15)
```

首先使用回归命令对父亲的身高和孩子年龄的虚拟变量进行孩子身高的回归：

```stata
regress height fheight Dage5 Dage10 Dage15
```

可以输入`test`命令，通过F检验来检验年龄在统计学上是显著的假设：

```stata
test Dage5 Dage10 Dage15
```

从 Stata 的 do 文件中是不能添加内容到 `ereturn`的宏指令的，因此如果有一个指令，可以以表格形式显示回归结果，显示形式类似`estimates table`，`outreg`和`xml_tab`，这样此时就可以轻松地在适当的行列中显示F检验的结果了。

而使用`addest`命令，就可以解决上述问题，分别对显示表中内容进行设定：

```stata
regress height fheight Dage5 Dage10 Dage15
  test Dage5 Dage10 Dage15
    addest, name("F_of_age") value(`r(F)')
    addest, name("p_of_age") value(`r(p)')
  est store onlyfather

regress height mheight Dage5 Dage10 Dage15
  test Dage5 Dage10 Dage15
    addest, name("F_of_age") value(`r(F)')
    addest, name("p_of_age") value(`r(p)')
  est store onlymother

regress height fheight mheight Dage5 Dage10 Dage15
  test Dage5 Dage10 Dage15
    addest, name("F_of_age") value(`r(F)')
    addest, name("p_of_age") value(`r(p)')
  test fheight=mheight
    addest, name("F_of_M_eq_F") value(`r(F)')
    addest, name("p_of_M_eq_F") value(`r(p)')
  est store both

```

使用`est table`命令得到显示结果：

```stata
est table onlyfather onlymother both, stat("F_of_age" "p_of_age" "F_of_M_eq_F" "p> _of_M_eq_F" F N r2)

```

最后 **estimates table **的表格展示如下：

```stata
-----------------------------------------------------
    Variable | onlyfather   onlymother      both     
-------------+---------------------------------------
     fheight |  .39378776                 .37366497  
       Dage5 |  2.5092039    2.8483203    2.5705705  
      Dage10 |  2.1560442    2.0992155    2.1577627  
      Dage15 |  1.5592282    1.6260293    1.5430182  
     mheight |               .31865049    .28892303  
       _cons |  35.971966    42.515225    18.790657  
-------------+---------------------------------------
    F_of_age |  55.871601    55.114653    58.924464  
    p_of_age |  4.201e-33    1.088e-32    9.309e-35  
 F_of_M_eq_F |                            1.7621445  
 p_of_M_eq_F |                            .18469688  
           F |  63.657074    52.551367    61.476023  
           N |        898          898          898  
          r2 |  .22187349    .19054063    .25628248  
-----------------------------------------------------

```

现在由用户编写的制表命令例如`esttab`、`outreg`和`xml_tab`等也可以访问和显示上述这些新的统计数据。

## 4. 结语

本篇推文主要介绍了`addest`命令的理论基础和部分功能的实际操作范例。`addest`的语法简单易懂，可操作性强，可设置选项较多，通过对于`ereturn`命令存储结果的拓展，使估计结果的适用性得到提高，并且`addest`命令中丰富的选项设置，丰富了`ereturn`的功能。