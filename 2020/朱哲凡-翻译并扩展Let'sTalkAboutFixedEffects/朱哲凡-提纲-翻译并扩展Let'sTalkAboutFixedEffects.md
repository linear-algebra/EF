&#x1F449;  点击右上方的【编辑】按钮，可以查看 Markdown 原始文档。

FE - 固定效应的那些事


> **作者：** 朱哲凡（中山大学）
>
> **E-mail：** <zhuzhf5@mail2.sysu.edu.cn> 



本篇推文介绍固定效应与其应用及缺陷。固定效应方法在实证计量中十分强大，其应用也十分广泛。而学者在使用固定效应方法的时候存在没有过多地考虑的问题，容易陷入了固定效应的某些误区之中。

## 1. 费？FE！固定效应介绍

## 2. 固定效应的缺陷

### 2.1 未观察到的异质性

### 2.2 群体差异

### 2.3 反向因果关系

### 2.4 外部有效性

## 3. 固定效应：未来的路在何方

## 4. 结语

## 5. 参考文献

- Collischon, M., Eberl, A. 2020. Let’s Talk About Fixed Effects: Let’s Talk About All the Good Things and the Bad Things. Köln Z Soziol 72, 289–299. [-Link-](https://link.springer.com/article/10.1007%2Fs11577-020-00699-8)，[-PDF-](https://www.jianguoyun.com/p/DYU0XLMQtKiFCBiR58wD)

- Hill, Terrence D., Andrew P. Davis, J. Micah Roos, and Michael T. French. 2020. “Limitations of Fixed-Effects Models for Panel Data.” Sociological Perspectives 63 (3): 357–69. [-Link-](https://academic.microsoft.com/paper/2963361005/citedby/search?q=Limitations%20of%20Fixed-Effects%20Models%20for%20Panel%20Data&qe=RId%253D2963361005&f=&orderBy=0)，[-PDF-](http://sci-hub.ren/10.1177/0731121419863785)

## 6. 前期相关推文

> Note：如下内容可以使用 `lianxh 固定效应, m` 命令自动生成。执行 `ssc install lianxh` 可以下载 `lianxh` 命令。 

- [Stata新命令：面板-LogitFE-ProbitFE](https://www.lianxh.cn/news/18a8416f25cce.html)
- [Stata：双重差分的固定效应模型-(DID)](https://www.lianxh.cn/news/f7499048842cc.html)
- [Stata新命令：ppmlhdfe-面板计数模型-多维固定效应泊松估计](https://www.lianxh.cn/news/c09d27b7de414.html)
- [Stata：非对称固定效应模型](https://www.lianxh.cn/news/b880417971c8c.html)
- [reghdfe：多维面板固定效应估计](https://www.lianxh.cn/news/8be6ff429cf93.html)
