&emsp;

「 **Sourse:** Cattaneo M. D., R. Titiunik, and G. Vazquez-Bare, Analysis of regression-discontinuity designs with multiple cutoffs or multiple scores, Stata Journal, 2020, 20(4): 866–891. [-PDF-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X20976320 )；
[-PDF1-](https://sci-hub.ren/10.1177/1536867X20976320 "-PDF1-") 」

&emsp;

> **作者**：林育莉  (中山大学)   
> **E-Mail:** <linyuliyx@163.com> 

&emsp;

&emsp;

---

**目录**
[TOC]

---

&emsp;

&emsp;

&emsp;

## 1. 简介

在经济学、政治学、公共学等许多学科的实证工作中，会遇到许多具有多断点或多配置变量的断点回归问题。这里将介绍 Stata 的  `rdmulti`，它由 `dmc`、`rdmcplot`、`rdms` 三个命令组成，可用于分析具有多断点和多配置变量的问题。

为了简化演示，这里只使用模拟数据来展示 `rdmulti` 涵盖的所有三个情形设定：非累积多断点、累积多断点和多配置变量。

-	非累积多断点 (Noncumulative multiple cutoffffs)：数据中每个单位都面临一个断点且不同单位的断点不相同。

-	累积多断点 (Cumulative multiple cutoffffs)：单位有一个配置变量和一系列有序的断点。

-	多配置变量 (Multiple scores)：政策处理由两个或者多个配置变量 (比如经度和纬度) 各自的断点决定。

&emsp;

## 2. 实例说明

### 2.1 非累积多断点

在这种情况下，模型中有且只有一个配置变量为 $X_{i}$，结果变量为向量 $\left(Y_{i}(0), Y_{i}(1)\right)$。而每一个单位有一个断点 $C_{i} \in \mathcal{C}$ ，其中 $\mathcal{C}=\left\{c_{1}, c_{2}, \ldots c_{J}\right\}$ 。例如，Chay，McEwan 和 Urquiola(2005) 研究了智利政府于 1990 年提出的一项学校改善计划的效果。在这项计划中，表现不佳的学校可获得公共资金用于改善基础设施额和师资培训等。该项目的分配是基于学校水平的测试配置变量低于断点值。在智利的 13 个行政区中，断点值是不同的。该例中，$C_{i}$ 代表每所学校的行政区域。

 与标准单断点 RD 方法不同，$C_{i}$ 是随机变量。当个体的配置变量超过相应的临界值时， $D_{i}=\mathbb{1}\left(X_{i} \geq C_{i}\right)$，将进行处理。该设计的关键特征是变量 $C_{i}$ 对总体进行划分，即每个单位面对一个且只有一个 $C_{i}$ 值。从标记看，无论他或她所面临的具体临界值是多少，每个人的潜在结果都是相同的；参见 Cattaneo 等 (2016，2020)。这里仅考虑有限多断点，因为这是实证工作的最自然的设置：在实践中，连续截断被离散用于估计和推断，如下所述。
 
在规则条件下，即包括了条件期望的平滑性，具体的断点处理效应$\tau(c)=$ $\mathbb{E}\left\{Y_{i}(1)-Y_{i}(0) \mid X_{i}=c, C_{i}=c\right\}$ 可定义为：

$$
\tau(c)=\lim _{x \downarrow c} \mathbb{E}\left(Y_{i} \mid X_{i}=x, C_{i}=c\right)-\lim _{x \uparrow c} \mathbb{E}\left(Y_{i} \mid X_{i}=x, C_{i}=c\right)
$$

混合 RD 估计 (Pooled RD estimate) 是通过对配置变量去断点化实现，$\tilde{X}_{i}=X_{i}$ $C_{i}$ ，从而使断点值归零。

$$
\tau_{\mathrm{P}}=\lim _{x \downarrow 0} \mathbb{E}\left(Y_{i} \mid \tilde{X}_{i}=x\right)-\lim _{x \uparrow 0} \mathbb{E}\left(Y_{i} \mid \tilde{X}_{i}=x\right)\quad (1)
$$

其中，

$$
\tau_{\mathrm{P}}=\sum_{c \in \mathcal{C}} \tau(c) \omega(c), \quad \omega(c)=\frac{f_{X \mid C}(c \mid c) \mathbb{P}\left(C_{i}=c\right)}{\sum_{c \in \mathcal{C}} f_{X \mid C}(c \mid c) \mathbb{P}\left(C_{i}=c\right)}
$$

所有这些参数都可以很容易通过使用局部多项式方法来估计 (有关实际介绍，请参阅 Cattaneo、Idrobo &Titiunik[2019])，并可以在适当的情况下对断点进行调整。即除了汇集数据外，RD 方法还可以单独应用于每个截止值。因此，`rdmultiple` 使用Calonico，Cattaneo&Titiunik(2014a，2015b) 和 Calonico 等人 (2017) 描述的 `rdrobust` 命令实现基于局部多项式方法的带宽选择、估计和推理。

混合参数 $\tau_{\mathrm{P}}$ 的权重是通过 $\omega(c)=\mathbb{P}\left(C_{i}=c \mid \tilde{X}_{i}=0\right) $  来进行估计的，详情可参见 Cattaneo  等 (2016)。如果给定带宽为  $h>0$，

$$
\widehat{\omega}(c)=\frac{\sum_{i} \mathbb{1}\left(C_{i}=c,-h \leq \tilde{X}_{i} \leq h\right)}{\sum_{i} \mathbb{1}\left(-h \leq \tilde{X}_{i} \leq h\right)}
$$

当用户没有指定时，`rdmc` 命令在估计混合效应时可使用 `rdrobust` 选择的带宽来估计权重。

### 2.2 累积多断点

在具有累积多断点模型中，个体对配置变量的不同范围接受不同的处理(或不同的处理程度)。在这种设定下，如果 $X_{i}$ 小于 $c_{1}$ 则接受处理 1，如果 $X_{i}$ 小于 $c_{2}$ 且大于或等于 $c_{1}$ 则接受处理 2，以此类推，直到最后的处理值满足 $X_{i}$ 不小于 $c_{J}$。

例如 Brollo 等 (2013) 审查联邦转移金额对巴西各城市政治腐败的影响。市政府获得的联邦转移金额取决于市政当局的人口，并在特定的断点上发生离散变化。例如，人口少于 10189 的城市获得一定数额，人口在 10189 到 13585 之间的城市获得较大数额，以此类推。

将这些处理的值记为 $d_{j}$，则处理变量为 $D_{i} \in\left\{d_{1}, d_{2}, \ldots d_{J}\right\}$。在标准的规律性条件下，有

$$
\tau_{j}=\mathbb{E}\left\{Y_{i}\left(d_{j}\right)-Y_{i}\left(d_{j-1}\right) \mid X_{i}=c_{j}\right\}=\lim _{x \downarrow c_{j}} \mathbb{E}\left(Y_{i} \mid X_{i}=x\right)-\lim _{x \uparrow c_{j}} \mathbb{E}\left(Y_{i} \mid X_{i}=x\right)
$$

与非累积多断点的情况不同，人口是不分区的，每个观察值可以用来估计两个不同的 (但在配置变量维度上是连续的) 处理效果。例如，接受处理 $d_{j}$ 的单位，在估计 $\tau_{j}$ 时作为“处理组” (即在断点 $c_{j}$ 之上)，在估计 $\tau_{j+1}$ 时作为“控制组” (即在断点 $c_{j+1}$ 之下)。因此，特定断点的估计量可能不是独立的，尽管只要每个断点周围的带宽随样本量减小，这种依赖就会逐渐消失。另一方面，带宽可以选择为不重叠，以确保观察值只使用一次。

一旦数据被分配到每个被分析的断点，局部多项式方法也可以应用于累积多断点情况下的断点。将会在下文说明这种方法。有关进一步讨论，请参阅 Cattaneo，Idrobo 和Titiunik(Forthcoming) 及其参考资料。

### 2.3 多配置变量

在多核心 RD 方法中，其处理时基于多个配置变量和一些确定处理“部位”或“区域”的函数来分配的。我们重点讨论了两个配置变量 $\mathbf{X}_{i}=\left(X_{1 i}, X_{2 i}\right)$ 的情况，这是迄今为止实证工作中最常见的情况。例如，当根据两次不同考试(如语言和数学)的配置变量进行处理时，这种情况自然发生。Matsudaira(2008) 估计了一项强制性的暑期学校计划的效果，该计划指定给那些在数学和语言考试中得分都不超过预设断点值的学生。多个配置变量的另一个常见情况发生在根据地理位置 (例如纬度和经度) 进行处理的时候。Keele&Titiunik(2015)通过比较相邻媒体市场的选民来讨论政治竞选广告对选民投票率和政治态度的影响，这会导致不同的广告曝光度。

这种类型的赋值定义了处理区域边界上的连续处理效应，用 $\mathcal{B}$ 表示。例如，如果将处理分配给在语言和数学上得分低于50的学生，则处理边界为 $\mathcal{B}=\left\{x_{1}\leq 50, x_{2}=50\right\} \cup\left\{x_{1}=50, x_{2} \leq 50\right\}$，该点的处理效应为 

$$
\tau(\mathbf{b})=\mathbb{E}\left\{Y_{i}(1)-Y_{i}(0) \mid \mathbf{X}_{i}=\mathbf{b}\right\}
$$ 

且在规则条件下，

$$
\tau(\mathbf{b})=\lim _{d(\mathbf{x}, \mathbf{b}) \rightarrow 0,} \mathbb{E}\left(Y_{i} \mid \mathbf{X}_{i}=\mathbf{x}\right)-\lim _{d(\mathbf{x}, \mathbf{b}) \rightarrow 0,} \mathbb{E}\left(Y_{i} \mid \mathbf{X}_{i}=\mathbf{x}\right)
$$

其中$\mathcal{B}_{c}$ 和 $\mathcal{B}_{t}$ 分别代表了控制区域和处理区域，$d(\cdot, \cdot)$ 是一个度量标准。

由于估计整个处理效应曲线在实践中可能是不可行的，因此通常定义一组感兴趣的边界点来估计 RD 处理效果。例如，在前面的例子中，边界确定处理分配的三个断点可以是 \{(25,50),(50,50),(50,25)\} 。另一方面，混合 RD 估计需要定义到断点的某种距离度量，例如垂直 (欧几里德) 距离。这个距离可以看作是最近的配置变量 $\tilde{X}_{i}$，它允许定义混合估计，如 (1) 中 所示。

&emsp;

## 3. 命令介绍
 
`rdmulti` 命令包的三个命令 `dmc`、`rdmcplot`、`rdms` 可通过 `help` 命令同时下载。例如：

```stata

help rdmc

```

### 3.1 rdmc

命令 `rdmc` 适用于非累积和累积多断点方法，可计算混合或特定断点的 RD 处理效应，使用局部多项式估计和提供稳健的 `robust` 偏差校正推理过程，允许事后估计和推断。

`rdmc` 命令的语法结构为：

```stata

rdmc depvar runvar [if] [in], ///
     cvar(cutoff_var) [fuzzy(string) ///
     derivvar(string) pooled_opt(string) ///
     verbose pvar(string) qvar(string) ///
     hvar(string) hrightvar(string) ///
     bvar(string) brightvar(string) ///
     rhovar(string) covsvar(string) ///
     covsdropvar(string) kernelvar(string) ///
     weightsvar(string) bwselectvar(string) ///
     scaleparvar(string) scaleregulvar(string) /// 
     masspointsvar(string) bwcheckvar(string) ///
     bwrestrictvar(string) stdvarsvar(string) ///
     vcevar(string) level(#) plot ///
     graph_opt(string)]

```

- `depvar`: 被解释变量。

- `runvar`：即 running variable，又叫score 和 forcing variable，中文翻译为配置变量、分配变量、驱动变量、分组变量等。

### 3.2 rdmcplot

命令 `rdmcplot` 是 `rdmc` 的配套命令，也可进行多断点回归，提供 RD 图形。`rdmcplot` 命令的语法结构为：

```stata
rdmcplot depvar runvar [if] [in], ///
         cvar(cutoff_var) [nbinsvar(string) ///
         nbinsrightvar(string)  ///
         binselectvar(string) scalevar(string) ///
         scalerightvar(string) ///
         supportvar(string) supportrightvar(string) ///
         pvar(string) hvar(string) /// 
         hrightvar(string) kernelvar(string) ///
         weightsvar(string) covsvar(string) ///
         covsevalvar(string) covsdropvar(string) ///
         binsoptvar(string) lineoptvar(string) ///
         xlineoptvar(string) ci(cilevel) ///
         nobins nopoly noxline nodraw ///
         genvars]
```

- `depvar`: 被解释变量。

- `runvar`：配置变量。


### 3.3 rdms

命令 `rdms` 适用于多配置变量 RDD，特别是包括累积断点和两个配置变量的环境设定。它基于局部多项式方法计算混合和特定断点的 RD 处理效应，并允许事后估计和推断。其语法结构为：

```stata

rdms depvar runvar1 [runvar2 treatvar] [if] [in], ///
     cvar(cutoff_var1[cutoff_var2]) 
     [range(range1 [range2]) xnorm(string) ///
     fuzzy(string) derivvar(string) ///
     pooled_opt(string) pvar(string) ///
     qvar(string) hvar(string) ///
     hrightvar(string) bvar(string) ///
     brightvar(string) rhovar(string) ///
     covsvar(string) covsdropvar(string) ///
     kernelvar(string) weightsvar(string) ///
     bwselectvar(string) scaleparvar(string) ///
     scaleregulvar(string) masspointsvar(string) ///
     bwcheckvar(string) bwrestrictvar(string) ///
     stdvarsvar(string) vcevar(string) ///
     level(#) plot graph_opt(string)]
     
```

- `depvar`：被解释变量。

- `runvar1`：累积断点设定中的运行变量 (或称配置变量、分配变量等) 。

- `runvar2`：若指定，则是二元配置变量设定中的第二运行变量。

- `treatvar`：若指定，则是二元配置变量设定中的处理指标。

&emsp;

## 4. Stata 实操

### 4.1 非累积多断点
\
我们首先使用模拟数据集simdata_multic.dta来说明 `rdmc`。在这个数据集中，$\mathrm{y}$ 是结果变量，$\mathrm{x}$ 是配置变量，$\mathrm{c}$ 是表示样本中每个单位对应的断点值的变量，$\mathrm{t}$ 是处理指标，在这种情况下对应于 $x \geq c$ 的单位。如下所示，有两个不同的断点值，即 33 和 66，每个都有相同的样本容量。

```stata

. use simdata_multic
. sum

    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
           c |      2,000        49.5    16.50413         33         66
           x |      2,000    50.79875    28.95934   .0184725   99.97507
           t |      2,000        .516    .4998689          0          1
           y |      2,000    1728.135    545.0856   540.0849   3015.232

```

`rdmc`的基本语法如下所示：

```stata

. rdmc y x ,cvar(c)

Cutoff-specific RD estimation with robust bias-corrected inference
--------------------------------------------------------------------------------
      Cutoff|    Coef.    P>|z|  [95% Conf. Int.]     hl     hr      Nh   Weight
------------+-------------------------------------------------------------------
          33|  484.831    0.00    421.18  552.53    14.66  14.66    289    0.540
          66|  297.981    0.00    220.35  362.27    11.95  11.95    246    0.460
------------+-------------------------------------------------------------------
    Weighted|  398.915    0.00    348.74  445.14        .      .    535        .
      Pooled|  436.400    0.00    179.34  676.63    13.68  13.68    550        .

```

输出结果显示每个断点的特定断点估计，以及相应的 robust 偏差校正 p 值、每个断点的95% robust 置信区间和样本大小以及两个“全局”估计。第一个是使用第 2 节中描述的估计权重的特定断点估计的加权平均值。这些估计权重显示在最后一列。第二个是通过规范配置变量获得的混合估计。虽然这两个估计量收敛到相同的总体参数，但它们在有限样本中可能有所不同，如上所示。在这个例子中，这两个断点上在统计学上是显著的。

上述显示中的所有结果都是使用 `rdrobust` 计算的。使用者可以为 `rdrobust` 指定 `option`，使用 `opt()` 计算混合估计。例如，下面的语法指定了 20 的带宽和混合估计的局部二次多项式。默认情况下，`rdmc` 在估计效果时省略了 `rdrobust` 的输出。混合效应估计的输出可以使用选项详细显示，我们在下面使用它来显示如何将 `option` 传递给 `rdrobust`。

```stata

. rdmc y x, cvar(c) pooled_opt(h(20) p(2)) verbose

Sharp RD estimates using local polynomial regression.

      Cutoff c = 0 | Left of c  Right of c            Number of obs =       2000
-------------------+----------------------            BW type       =     Manual
     Number of obs |       968        1032            Kernel        = Triangular
Eff. Number of obs |       409         416            VCE method    =         NN
    Order est. (p) |         2           2
    Order bias (q) |         3           3
       BW est. (h) |    20.000      20.000
       BW bias (b) |    20.000      20.000
         rho (h/b) |     1.000       1.000

Outcome: y. Running variable: __000002.
--------------------------------------------------------------------------------
            Method |   Coef.    Std. Err.    z     P>|z|    [95% Conf. Interval]
-------------------+------------------------------------------------------------
      Conventional |  437.04      129.8   3.3671   0.001    182.643      691.441
            Robust |     -          -     3.0118   0.003    185.618      877.381
--------------------------------------------------------------------------------


Cutoff-specific RD estimation with robust bias-corrected inference
--------------------------------------------------------------------------------
      Cutoff|    Coef.    P>|z|  [95% Conf. Int.]     hl     hr      Nh   Weight
------------+-------------------------------------------------------------------
          33|  484.831    0.00    421.18  552.53    14.66  14.66    289    0.540
          66|  297.981    0.00    220.35  362.27    11.95  11.95    246    0.460
------------+-------------------------------------------------------------------
    Weighted|  398.915    0.00    348.74  445.14        .      .    535        .
      Pooled|  437.042    0.00    185.62  877.38    20.00  20.00    825        .

```

用户还可以修改每个特定截断值中的估计选项。下面的语法显示了如何通过在第一个截止值中设置 11 的带宽和在第二个截止值中设置 10 来手动更改特定截止值的估计选项。

```stata

. generate double h = 11 in 1
(1,999 missing values generated)

. replace h = 10 in 2
(1 real change made)

. rdmc y x, cvar(c) hvar(h)


Cutoff-specific RD estimation with robust bias-corrected inference
--------------------------------------------------------------------------------
      Cutoff|    Coef.    P>|z|  [95% Conf. Int.]     hl     hr      Nh   Weight
------------+-------------------------------------------------------------------
          33|  495.429    0.00    368.13  563.21    11.00  11.00    207    0.498
          66|  303.769    0.00    220.40  403.32    10.00  10.00    209    0.502
------------+-------------------------------------------------------------------
    Weighted|  399.138    0.00    321.56  455.23        .      .    416        .
      Pooled|  436.400    0.00    179.34  676.63    13.68  13.68    550        .

```

所有特定断点的选项都以类似的方式传递。定义一个新的长度变量，令它等于截断值，该变量指示其值中每个截止值的选项。以下语法指示在每个截止点不同的带宽选择方法。

```stata

. generate bwselect = "msetwo" in 1
(1,999 missing values generated)

. replace bwselect = "certwo" in 2
(1 real change made)

. rdmc y x, cvar(c) bwselectvar(bwselect)


Cutoff-specific RD estimation with robust bias-corrected inference
--------------------------------------------------------------------------------
      Cutoff|    Coef.    P>|z|  [95% Conf. Int.]     hl     hr      Nh   Weight
------------+-------------------------------------------------------------------
          33|  481.135    0.00    418.03  545.23    15.03  16.91    318    0.572
          66|  298.835    0.00    227.70  367.26    14.85   7.95    238    0.428
------------+-------------------------------------------------------------------
    Weighted|  403.100    0.00    355.73  449.87        .      .    556        .
      Pooled|  436.400    0.00    179.34  676.63    13.68  13.68    550        .

```

`rdmc` 命令在矩阵 $e(b)$ 和 $e(V)$ 中保存了偏差校正估计和方差，这允许使用 `lincom` 或 `test` 进行估计后检验。例如，要检验两个断点处的效果是否相同，可输入

```stata

. rdmc y x, cvar(c)


Cutoff-specific RD estimation with robust bias-corrected inference
--------------------------------------------------------------------------------
      Cutoff|    Coef.    P>|z|  [95% Conf. Int.]     hl     hr      Nh   Weight
------------+-------------------------------------------------------------------
          33|  484.831    0.00    421.18  552.53    14.66  14.66    289    0.540
          66|  297.981    0.00    220.35  362.27    11.95  11.95    246    0.460
------------+-------------------------------------------------------------------
    Weighted|  398.915    0.00    348.74  445.14        .      .    535        .
      Pooled|  436.400    0.00    179.34  676.63    13.68  13.68    550        .
--------------------------------------------------------------------------------

. matlist e(b)

             |        c1         c2   weighted     pooled 
-------------+--------------------------------------------
          y1 |  486.8578   291.3082   396.9415   427.9832 

. lincom c1-c2

 ( 1)  c1 - c2 = 0

------------------------------------------------------------------------------
             |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
         (1) |    195.550     49.331    3.964   0.000       98.863     292.236

```

`rdmcplot` 命令联合绘制每个断点的估计回归函数，其输出如图1所示。

```stata

. rdmcplot y x, cvar(c)

```

&emsp;

![Figure 1. Multiple RD plot](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/RDD-多断点_Fig1_多断点曲线_林育莉.png)

&emsp;


`rdmcplot` 包括了 `rdplot` 的所有选项。例如，绘图可以限制在使用选项 `hvar()` 的带宽，并使用通过选项 `pvar()` 指定顺序的多项式，如下所示。此选项允许用户在每个断点绘制线性拟合和估计处理效应。

```stata

. generate p = 1 in 1/2
(1,998 missing values generated)

. rdmcplot y x, cvar(c) hvar(h) pvar(p)

```
&emsp;

结果如图2 所示：

&emsp;

![Figure 2. Multiple RD plot](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/RDD-多断点_Fig2_多断点曲线_林育莉.png)

&emsp;

选项 `genvars` 生成手工复制绘图所需的变量。这允许用户自定义绘图。下面的代码说明了如何使用此选项复制图2。

```stata

. rdmcplot y x, cvar(c) genvars
. twoway (scatter rdmcplot_mean_y_1 rdmcplot_mean_x_1, mcolor(navy))
> (line rdmcplot_hat_y_1 rdmcplot_mean_x_1 if t==1, sort lcolor(navy))
> (line rdmcplot_hat_y_1 rdmcplot_mean_x_1 if t==0, sort lcolor(navy))
> (scatter rdmcplot_mean_y_2 rdmcplot_mean_x_2, mcolor(maroon))
> (line rdmcplot_hat_y_2 rdmcplot_mean_x_2 if t==1, sort lcolor(maroon))
> (line rdmcplot_hat_y_2 rdmcplot_mean_x_2 if t==0, sort lcolor(maroon)),
> xline(33, lcolor(navy) lpattern(dash))
> xline(66, lcolor(maroon) lpattern(dash))
> legend(off)

```

### 4.2 累积多断点

我们通过模拟数据集 simdata_cumul.dta 来说明使用 `rdms` 进行累积截断。在此数据集中，配置变量范围为从 0 到 100，配置变量低于 33 的单位接受处理级别 $d_{1}$，而配置变量高于 66 的单位接收另一个处理级别 $d_{2}$。在此设定中，断点值在数据集中表示为变量，其中每一行表示一个断点值。

```stata

. use simdata_cumul, clear

. sum

    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
           x |      1,000    50.46639    28.69369   .0413166    99.8783
           y |      1,000    1508.638    488.2752   658.4198   2480.568
           c |          2        49.5    23.33452         33         66
. tabulate c

          c |      Freq.     Percent        Cum.
------------+-----------------------------------
         33 |          1       50.00       50.00
         66 |          1       50.00      100.00
------------+-----------------------------------
      Total |          2      100.00

```

累积截断的语法类似于 `rdmc`。用户指定的结果变量、配置变量和断点如下：

```stata

. rdms y x, cvar(c)

Cutoff-specific RD estimation with robust bias-corrected inference
--------------------------------------------------------------------------------
         Cutoff|      Coef.     P>|z|     [95% Conf. Int.]     hl     hr      Nh
---------------+----------------------------------------------------------------
             33|    395.492     0.000     363.76   423.86    15.11  15.11    286
             66|    342.872     0.000     315.95   373.96    12.22  12.22    265
--------------------------------------------------------------------------------
```

每个特定断点效应的带宽、多项式顺序和核心等选项可以通过创建变量来指定，如下所示。

```stata

. generate double h = 11 in 1
(999 missing values generated)

. replace h = 8 in 2
(1 real change made)

. generate kernel = "uniform" in 1
(999 missing values generated)

. replace kernel = "triangular" in 2
variable kernel was str7 now str10
(1 real change made)

. rdms y x, cvar(c) hvar(h) kernelvar(kernel)

Cutoff-specific RD estimation with robust bias-corrected inference
--------------------------------------------------------------------------------
         Cutoff|      Coef.     P>|z|     [95% Conf. Int.]     hl     hr      Nh
---------------+----------------------------------------------------------------
             33|    394.470     0.000     351.65   438.72    11.00  11.00    215
             66|    342.505     0.000     301.56   375.95     8.00   8.00    166
--------------------------------------------------------------------------------
```

如果没有进一步的信息，`rdms` 命令可以使用断点 33 以上的任何观察值来估计第一个处理级别 $d_{1}$ 的影响。这意味着使用了 [66,100] 范围内的一些观察值。但这些观察值接受第二个处理级别 $d_{2}$。这一特性可能导致 $\tau_{1}$ 的估计不一致。为了避免这个问题，用户可以指定每个截止点周围要使用的观察值范围。在这种情况下，我们可以将第一个断点 (33) 的范围限制在0到65.5之间，以确保不使用超过66的观测，第二个截止点 (66) 的范围从33.5到100。方法如下：

```stata

. generate double range_l = 0 in 1
(999 missing values generated)

. generate double range_r = 65.5 in 1
(999 missing values generated)

. replace range_l = 33.5 in 2
(1 real change made)

. replace range_r = 100 in 2
(1 real change made)

. rdms y x, cvar(c) range(range_l range_r)


Cutoff-specific RD estimation with robust bias-corrected inference
--------------------------------------------------------------------------------
         Cutoff|      Coef.     P>|z|     [95% Conf. Int.]     hl     hr      Nh
---------------+----------------------------------------------------------------
             33|    394.698     0.000     356.12   430.45    10.96  10.96    214
             66|    342.180     0.000     312.20   372.04    11.18  11.18    246

```

混合估计可以通过 `rdmc` 获得。为此，我们需要为断点值分配样本中的每个单位。一种可能性是将每个单位分配到最近的断点值。为此，我们生成一个名为**断点**的变量。对于低于49.5的单位 (33和66之间的中点)，**断点**等于 33，对于高于 49.5 的单位，**断点**等于66。

```stata

. generate double cutoff = c[1]*(x<=49.5) + c[2]*(x>49.5)

. rdmc y x, cvar(cutoff)


Cutoff-specific RD estimation with robust bias-corrected inference
--------------------------------------------------------------------------------
      Cutoff|    Coef.    P>|z|  [95% Conf. Int.]     hl     hr      Nh   Weight
------------+-------------------------------------------------------------------
          33|  389.528    0.00    332.94  443.69     6.26   6.26    119    0.531
          66|  341.015    0.00    300.39  377.33     5.04   5.04    105    0.469
------------+-------------------------------------------------------------------
    Weighted|  366.788    0.00    330.63  399.64        .      .    224        .
      Pooled|  363.968    0.00    180.11  551.78     8.14   8.14    333        .

```

最后，我们可以使用**断点**变量通过命令 `rdmcplot` 绘制回归函数，如图 3 所示。

```stata

. generate binsopt = "mcolor(navy)" in 1/2
(998 missing values generated)

. generate xlineopt = "lcolor(navy) lpattern(dash)" in 1/2
(998 missing values generated)

. rdmcplot y x, cvar(cutoff) binsoptvar(binsopt) xlineopt(xlineopt) nopoly
```

&emsp;

![Figure 3. Cumulative cutoffs](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/RDD-多断点_Fig3_累积多断点_林育莉.png)

&emsp;


### 4.3 多配置变量

我们现在利用模拟数据集 simdata_multis.dta 来说明使用 `rdms` 对两个运行变量的 RD 设计进行分析。在这个数据集中，有两个运行变量，分别为 $x 1$ 和 $x 2$，其范围在0到100之间。当 $\mathrm{x} 1 \leq 50$ 和 $\mathrm{x} 2 \leq 50$ 时，该单位接受处理。边界上的三个截断点：(25,50)，(50,50)和(50,25)。

```stata

. use simdata_multis, clear

. summarize

    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
          x1 |      1,000    50.22881    28.87868   .6323666   99.94879
          x2 |      1,000    50.63572     29.1905   .0775479   99.78458
           t |      1,000        .223    .4164666          0          1
           y |      1,000    728.5048    205.5627   329.4558   1372.777
          c1 |          3    41.66667    14.43376         25         50
-------------+---------------------------------------------------------
          c2 |          3    41.66667    14.43376         25         50

. list c1 c2 in 1/3

     +---------+
     | c1   c2 |
     |---------|
  1. | 25   50 |
  2. | 50   50 |
  3. | 50   25 |
     +---------+

```

下面的代码提供了这个设置的简单可视化，如图 4 所示：

```stata
. generate xaux = 50 in 1/50
(950 missing values generated)

. generate yaux = _n in 1/50
(950 missing values generated)

. twoway (scatter x2 x1 if t==0, msize(small) mfcolor(white) msymbol(X))
>        (scatter x2 x1 if t==1, msize(small) mfcolor(white) msymbol(T))
>        (function y = 50, range(0 50) lcolor(black) lwidth(medthick))  
>        (line yaux xaux, lcolor(black) lwidth(medthick))               
>        (scatteri 50 25, msize(large) mcolor(black))                   
>        (scatteri 50 50, msize(large) mcolor(black))                   
>        (scatteri 25 50, msize(large) mcolor(black)),                  
>        text(25 25 "Treated", size(vlarge))                            
>        text(60 60 "Control", size(vlarge))                            
>        legend(off)

```

&emsp;

![Figure 4. Bivariate score](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/RDD-多断点_Fig4_二元驱动变量_林育莉.png)

&emsp;


基本语法如下：

```stata

. rdms y x1 x2 t, cvar(c1 c2)


Cutoff-specific RD estimation with robust bias-corrected inference
--------------------------------------------------------------------------------
         Cutoff|      Coef.     P>|z|     [95% Conf. Int.]     hl     hr      Nh
---------------+----------------------------------------------------------------
        (25,50)|    243.842     0.111     -50.93   491.18    11.12  11.12     42
        (50,50)|    578.691     0.000     410.83   764.88    13.83  13.83     47
        (50,25)|    722.444     0.000     451.49  1060.15    10.83  10.83     38

```

估计每个特定断点估计的信息可以通过前文获取。例如，要指定特定断点的带宽，可输入

```stata

. generate double h = 15 in 1
(999 missing values generated)

. replace h = 13 in 2
(1 real change made)

. replace h = 17 in 3
(1 real change made)

. rdms y x1 x2 t, cvar(c1 c2) hvar(h)


Cutoff-specific RD estimation with robust bias-corrected inference
--------------------------------------------------------------------------------
         Cutoff|      Coef.     P>|z|     [95% Conf. Int.]     hl     hr      Nh
---------------+----------------------------------------------------------------
        (25,50)|    336.121     0.233    -119.35   491.36    15.00  15.00     87
        (50,50)|    583.047     0.000     501.94  1101.24    13.00  13.00     42
        (50,25)|    620.692     0.000     464.92  1159.99    17.00  17.00     86

```

最后，`xnorm()` 选项允许用户指定规范化的配置变量来计算混合估计。在这种情况下，我们将标准化驱动变量定义为与处理分配所定义的边界最接近的垂直距离，正值表示处理单位，负值表示控制单位。

```stata

. generate double aux1 = abs(50 - x1)

. generate double aux2 = abs(50 - x2)

. egen xnorm = rowmin(aux1 aux2)

. replace xnorm = xnorm*(2*t-1)
(777 real changes made)

. rdms y x1 x2 t, cvar(c1 c2) xnorm(xnorm)

Cutoff-specific RD estimation with robust bias-corrected inference
--------------------------------------------------------------------------------
         Cutoff|      Coef.     P>|z|     [95% Conf. Int.]     hl     hr      Nh
---------------+----------------------------------------------------------------
        (25,50)|    243.842     0.111     -50.93   491.18    11.12  11.12     42
        (50,50)|    578.691     0.000     410.83   764.88    13.83  13.83     47
        (50,25)|    722.444     0.000     451.49  1060.15    10.83  10.83     38
---------------+----------------------------------------------------------------
         Pooled|    447.017     0.000     389.33   496.85    12.73  12.73    433

```

&emsp;

## 5. 程序和补充材料

要获取相应的论文及论文的程序和数据，可输入：
```stata
. net sj 20-4
. net install st0620
. net get st0620
```
