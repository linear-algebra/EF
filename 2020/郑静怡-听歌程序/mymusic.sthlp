{smcl}
{* *! version 1 14Dec2020}{...}
{cmd:help for mymusic}
{hline}

{title:Title}

    mymusic - find music while working on Stata

{title:Require}

{pstd}To use this command, you will need to have installed sxpose and jsonio, which is available from ssc via the following link

{pstd}{stata ssc install sxpose}{p_end}
{pstd}{stata ssc install jsonio}{p_end}


{title:Syntax}

{pstd}syntax [, NAME(string)] [PLATFORM(string)] [BROWSE] [STOP] [KIND(string)]
	
{title:Description}

{pstd}mucistool--you can search what you would like to listen and play it online or locally in Stata without affecting your work.

{title:Options}

{synoptset 20 tabbed}{...}
{synopthdr}
{synoptline}
{syntab:{it:Optionals}...}

{synopt:{opt  name}}Decide which song you would like to hear, default is random song on daily soaring list in Netease.{p_end}

{synopt:{opt  plat:form(string)}}Specify music platform (Tencent, Netease or Local(support all uppercase, all lowercase)); default is Netease.{p_end}
{p 26 4 2}if platform=local, will need to global songpath.{p_end}
			
{synopt:{opt  br:owse}}Open the link directly in your web browser,when platform!=Local{p_end}

{synopt:{opt  stop}}Stop the music if platform=local, only for Windows(groove.exe or QQMusic.exe){p_end}

{synopt:{opt  kind}}Specify which type you're searching, default is single.{p_end}
{p 26 4 2}for Netease: single=1; album=10; songlist=1000; MV=1004; lyrics=1006{p_end}
{p 26 4 2}for Tencent: single=0; album=8; MV=12; lyrics=7{p_end}
					
{synoptline}
{p2colreset}{...}										
					
{title:Examples}

. mymusic, name(所念皆星河)
 
所念皆星河 辗转里反侧
你占领每个 永恒的片刻
无垠的宇宙 浩瀚的选择
你是最亮那颗
所爱如月色 触手而不得
将温柔的梦 都投射
你眼里有我 对这世间的
吝啬
 <<所念皆星河>>, by 房东的猫
 https://music.163.com/#/song?id=1476239407

. mymusic, name(云与海) platform(Tencent)
 如果世间万物能跨越能相爱
 也能成全云与海
 忘了离岸多远多危险
 都看不见
 如果海角天涯不分开不难捱
 眼泪终会厮守
 别忘了 它们的爱而 不得
 <<云与海>>, by 阿YueYue
 http://ws.stream.qqmusic.qq.com/C400004IFjwP42XBGI.m4a?guid=358840384&vkey=176F63D7435AE2E41D9867248F3327F953476AA5861BC4C6D4BC358B2868093DBA1C560E661ECA5754FCDAAC04D93CE62DEB0DF37F02934E&uin=0&fromtag=66
			
. mymusic, name(柴小协)

This is a pure music without lyrics, please enjoy.
 <<Violin Concerto in D, Op. 35:Ⅰ.Allegro moderator>>, by Jascha Heifetz
 https://music.163.com/#/song?id=1604782
 
. mymusic, name(Panorama) platform(local)
请定义音乐存放路径(global songpath),并以\结尾
//rerun command after global will automatically open your default music player and play music
			
{title:Shortcoming}

{pstd}
platform(local) is not available when Mac system or default music player is Netease
	

{title:Author}

{phang}
{cmd:Jingyi, Zheng (郑静怡)} Lingnan College, Sun Yat-Sen University, China.{break}
E-mail: {browse "mailto:arlionn@163.com":arlionn@163.com}. {break}
Blog: {browse "lianxh.cn":https://www.lianxh.cn}. {break}
{p_end}

{phang}
{cmd:Yujun, Lian* (连玉君)} Lingnan College, Sun Yat-Sen University, China.{break}
E-mail: {browse "mailto:arlionn@163.com":arlionn@163.com}. {break}
Blog: {browse "lianxh.cn":https://www.lianxh.cn}. {break}
{p_end}


{title:For problems and suggestions}

{p 4 4 2}
Any problems or suggestions are welcome, please Email to
{browse "mailto:arlionn@163.com":arlionn@163.com}. 

{p 4 4 2}	
You can also submit keywords you can't find and suggestions here:
{browse "https://www.wjx.cn/jq/98072236.aspx":[Click]}.

{p 4 4 2}
The latest version of package can be obtained from the project home page: {browse "https://gitee.com/arlionn/lianxh":https://gitee.com/arlionn/lianxh}
	