# Stata机器学习分类器

写一篇推文，介绍 `c_ml_stata` 命令。需要在 Stata 16.1 上运行

## 简介

`c_ml_stata` is a command for implementing machine learning classification algorithms in Stata 16. It uses the Stata/Python integration (sfi) capability of Stata 16 and allows to implement the following classification algorithms:

- tree,
- boosting,
- random forest,
- regularized multinomial,
- neural network,
- naive Bayes,
- nearest neighbor,
- support vector machine.

It provides hyper-parameters' optimal tuning via K-fold cross-validation using greed search.

For each observation (or instance), this command generates both predicted class probabilities and predicted labels using the Bayes classification rule. This command makes use of the Python Scikit-learn API to carry out both cross-validation and prediction.

## 命令的安装

输入如下命令，查看完整程序文件和相关附件。

```stata
. net describe c_ml_stata
```

## Stata 实操

## 参考文献