# rqrs命令介绍
> **作者：** 陈俊睿 (中山大学)        
> **E-mail:**  <512979356@qq.com>   

[toc]

## 1. 简介
`rqrs` 可用于在 do-file 里加载 stata 命令，主要用于加载社区提供的命令(community-contributed command)。`rqrs` 会检查在ado路径中是否找到了指定的社区命令，如果没有找到，则下载并安装包含该命令的所需包。

## 2. rqrs 语法介绍
### 2.1 基础语法介绍

```
rqrs command [ pkgname ] [ , options ] 
```
`command` 是所要安装的社区命令的名字；
`pkgname` 是目标命令所需包的名称，通常可忽略。

### 2.2 option 介绍
- `from(directory_or_url) ` 在括号内可指定社区命令来源。来源可以是文件目录或特定网址，也可以是以下短命令:

| 短命令  | 下载安装来源网址  |
|:----------|:---------------|
| ssc    | SSC(默认，若from内无参数也为SSC)    |
| net    | 从net设置的位置    |
| sj#-#  | Stata Journal (SJ) vol-issue    |
| stb#   | Stata技术公告(STB)   |
- `all` 下载与软件包相关的任何辅助文件。
- `replace` 用下载的文件替换现有文件。注意，在执行 `all` 与 `replace` 命令时，如果所需的命令已经安装并在ado路径中找到，则 `all` 与 `replace` 命令不会被执行,若强制执行需加上 `force`。
- `force` 强制下载并安装所需的包。若所需的命令已经安装并在ado路径中找到，则进行强制替换。
- `install` 默认情况下，在下载和安装所需的软件包之前会询问是否进行安装；若加上`install` ，安装之前将不再询问。如果所需的命令已经安装并在ado路径中能找到，则 `install` 不会被执行。

### 2.3 rqrs 下载方法
```
ssc install rqrs 
```

## 3. rqrs 应用
假设需要在dofile里载入官方命令 `findname` 、 `esttab` (来源于包 `estout` )；以及社区命令 `tbl2frmt`。

```
version 15.1
capture noisily which rqrs  	//判断是否已经安装rqrs
if ( _rc ) ssc install rqrs 	//安装rqrs

rqrs findname, install		//利用rqrs安装findname，并取消询问许可
//rqrs findname dm0048_4 , from(sj20-2)	//也可以指定安装最新版本的命令

rqrs esttab estout		//利用rqrs安装estout包里的esttab命令

rqrs tbl2frmt,from(http://digital.cgdev.org/doc/stata/MO/Misc)
				//安装来源于特定网址的命令
...

findname ...			//命令调用……
esttab ...
tbl2frmt ...
```

上面的 do-file 自动下载并安装所有必需的社区提供的命令。值得注意的是，在安装社区命令之前必须确保 `rqrs` 命令已经安装。

## 4.补充信息
原作者信息：
- Daniel Klein
- University of Kassel
- klein.daniel.81@gmail.com

