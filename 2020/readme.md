## 选题登记列表

> Update: `2020/12/22 11:25`

> &#x1F449;  [请在此登记你的选题](https://gitee.com/arlionn/EF/blob/master/2020/___%E9%80%89%E9%A2%98%E7%99%BB%E8%AE%B0%E5%88%97%E8%A1%A8___.md)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)


## 展示：已发布的课程报告

> update：`2021/1/6 9:26`

- 郑皓文：[ZIP-too many Zero：零膨胀泊松回归模型](https://www.lianxh.cn/news/2144066aaa0b4.html)
- 张翊：[Stata-RIF回归和RIF分解：探究收入不平等性的成因](https://www.lianxh.cn/news/a6b816bd39ace.html)
- 康峻杰，刘庆庆：[ssc install lianxh：你想要的 Stata 都在这里](https://www.lianxh.cn/news/ebcefeafd21e5.html)
- 余影：[Stata程序：10 分钟快乐编写 ado 文件](https://www.lianxh.cn/news/a9d7de7ff1d80.html)


&emsp;


## 课程报告登记提交说明

1. 申请一个码云账号，<https://gitee.com/>
2. 点击 [【邀请码】](https://gitee.com/arlionn/EF/invite_link?invite=99d36473e26408ba4d9f9b986084c92b82ba9cf4db3847b3b7993663152babc5413c4739c268cb3d6121c5559972f7e3) 获取课程报告提交和编辑权限。
  <https://gitee.com/arlionn/EF/invite_link?invite=99d36473e26408ba4d9f9b986084c92b82ba9cf4db3847b3b7993663152babc5413c4739c268cb3d6121c5559972f7e3>
  - 提示：邀请码有效期至 `2020/12/31 23:21`，请尽快加入。
3. 请在 [【EF/2020】](https://gitee.com/arlionn/EF/tree/master/2020) 文件夹下新建一个文件夹，名称为【姓名-课程报告标题简称】，如【张帅帅-面板平滑系数模型】。
4. 在你的个人文件夹下添加文件：
  - **A. 课程报告提纲** 新建文件，名称为【姓名-提纲-课程报告标题简称.md】，然后把你的推文提纲贴入其中。注意：(1) 新建文档务必添加后缀【**.md**】，否则无法正常显示。(2) 请用 VScode 或 Typora 等具有实时保存功能的编辑器在本地撰写报告，不要直接在码云上写，容易丢失。
  - **B. 课程报告正文** 新建文件，名称为【姓名-正文-课程报告标题简称.md】，其他要求同上。 
5. 在码云提交后，请同时通过坚果云提交一份 &#x1F449;  [点击提交](https://workspace.jianguoyun.com/inbox/collect/8b71e5254a0f4f36ac95b582ffd7f2db/submit)。如需更新，只需再次提交即可，版本号递增，即 **v2**， **v3**，…… 。 

## 其他选题
大家也可以通过 [论文重现网站](https://www.lianxh.cn/news/e87e5976686d5.html)，尤其是 JFE，JF，RFS 等金融类期刊，找一些 Top 期刊论文来复现，但开始前要征得我的同意，以免撞车。 

