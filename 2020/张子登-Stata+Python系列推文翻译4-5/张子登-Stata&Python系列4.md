&emsp;

> **作者：** 张子登 (中山大学)   
> **邮箱：** <zhangzd5@mail2.sysu.edu.cn>

&emsp;

&emsp;

>**编者按：** 本文摘自以下文章，特此感谢！  
>**Source:** The Stata Blog: Stata/Python integration part 4: How to use Python packages
[-Link-](https://blog.stata.com/2020/09/10/stata-python-integration-part-4-how-to-use-python-packages/)

&emsp;

&emsp;

---

**目录**
[TOC]

---

&emsp;

&emsp;

## 1. 引言

在上篇推文中，我们展示了如何运用 pip 安装四个受欢迎的 Python 软件包。
今天，我们将展示关于如何导入和使用 Python 软件包的基础知识，并学习一些重要的 Python 概念和术语。

在下面的示例中，我们将使用 pandas 包用以距离，但其理论和语法同样适用于其他 Python 软件包。

&emsp;

## 2. 导入模块以使用软件包

 [pandas](https://pandas.pydata.org/) 是一款流行的用于导入、导出和处理数据的 Python 软件包。该软件包包含了用于处理不同结构数据的不同模块，例如数组 (Series)， 数据框 (DataFrame) 和面板数据 (Panels)。

 首先，我们输入 `python which pandas`  来检查系统中是否已安装 pandas。

```python
. python which pandas
<module 'pandas' from 'C:\\Users\\ChuckStata\\AppData\\Local\\Programs\\Python\\
> Python38\\lib\\site-packages\\pandas\\__init__.py'>
```

如果在结果中看不到该路径，则需按照之前推文中的说明安装 pandas 软件包。

接下来，我们可以通过在代码块开头键入 `import pandas` 告诉 Python 我们想要使用 pandas 包。这将从包中导入 pandas 模块。

```python
python
import pandas
end
```

然后，我们可以使用 `pandas.read_stata()` 方法从 Stata Press 网站中将 Stata 的自带数据集读取到名为 “auto” 的 pandas 数据框中。

```python
python
import pandas
auto = pandas.read_stata("http://www.stata-press.com/data/r16/auto.dta")
end
```


术语**方法**用以描述模块内的一个函数。例如， pandas 包中的 `read_stata()` 方法是用来读取 Stata 数据集并将其转换为 pandas 数据框的函数。

&emsp;

## 3. 使用别名导入模块

pandas模块包含许多方法，最终我们可能会厌倦在每种方法之前键入  
`pandas`。我们可以通过给模块名称起别名来避免键入冗长的模块名称,输入 `import "模块名称" as "别名"`。 


在下面的代码块中，我们输入了 `import pandas as pd` ，将别名 `pd` 命名给 `pandas`。 现在，我可以通过键入 `pd.read_stata()` 而不是 `pandas.read_stata()` 来使用 `read_stata()` 方法。

```python
python
import pandas as pd
auto = pd.read_stata("http://www.stata-press.com/data/r16/auto.dta")
end
```

&emsp;

## 4. 使用模块中的方法和类

模块是一种针对不同应用情况细分软件包功能的途径。 一个模块可以定义一组方法、类和变量。 我们可以在 Python 语句的模块中引用它们。 例如， `DataFrame` 类包含在 pandas 包中的 `pandas.core.frame` 模块中。 

通常，我们仅引用包中的类并省略模块名称。 例如，下面代码块中的第四行使用 pandas 包中 `DataFrame` 类的 `mean()` 方法来估算 **mpg** 和 **weight** 的均值。

```python
python
import pandas as pd
auto = pd.read_stata("http://www.stata-press.com/data/r16/auto.dta")
pd.DataFrame.mean(auto[['mpg','weight']])
end
```

上面的代码块输出以下结果：

```python
. python
----------------------------------------------- python (type end to exit) ------
>>> import pandas as pd
>>> auto = pd.read_stata("http://www.stata-press.com/data/r16/auto.dta")
>>> pd.DataFrame.mean(auto[['mpg','weight']])
mpg         21.297297
weight    3019.459459
dtype: float64
>>> end
--------------------------------------------------------------------------------
```

&emsp;

## 5. 从模块中导入方法和类

您也可以从包中的模块直接导入类，并在使用类时省略掉模块名称或别名。 

下面代码块的第三行从 pandas 包中导入 `DataFrame` 类 (请注意，大写很重要) 。 现在，您可以通过键入 `DataFrame.mean()` 而不是 `pd.DataFrame.mean()` 来使用 `mean()` 方法。

```python
python
import pandas as pd
from pandas import DataFrame
auto = pd.read_stata("http://www.stata-press.com/data/r16/auto.dta")
DataFrame.mean(auto[['mpg','weight']])
end
```

&emsp;

## 6. 用别名导入函数和类

您可能会厌倦键入 `DataFrame.mean()` 在每次估计平均值时。幸运的是，您可以通过键入 `from "模块名称" import "类名" as "别名"` 来为类分配别名。 

在下面的代码块的第三行中，我们通过键入 `from pandas import DataFrame as df` 将别名 `df` 命名给 `DataFrame` 类。 现在，我可以通过键入 `df.mean()` 而不是 `DataFrame.mean()` 来使用 `mean()` 方法。

```python
python
import pandas as pd
from pandas import DataFrame as df
auto = pd.read_stata("http://www.stata-press.com/data/r16/auto.dta")
df.mean(auto[['mpg','weight']])
end
```

&emsp;

## 7. 回顾与总结

我们用下图简单回顾所学到的概念和术语：
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/张子登-Python&Stata系列4-Fig1.png)

Python 软件包是模块的集合。 每个模块可以包含一组方法，类和变量。 例如， pandas 软件包包含了一系列用于导入、导出和管理数据的类和方法。

方法是一种可以接受参数并执行某些操作的函数。 方法可以是模块或类的一部分。

包中的方法通常划分在模块和类中。 包和类可以分别包含许多方法。 我们必须先导入模块或类，然后才能使用它们的方法。

您可以在 [pandas 用户指南](https://pandas.pydata.org/docs/user_guide/index.html#user-guide) 中阅读有关 pandas 软件包的更多信息。

至此，我们已经奠定了所需的所有基础，并准备好进入更加有趣的部分！ 在下一篇文章中，我们将向您展示如何使用 Stata 估计逻辑回归模型中的边际预测，以及如何使用 Python 绘制该预测的三维表面图。 