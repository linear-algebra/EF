*****************************************************
* feologit 应用案例：第一个孩子对母亲生活满意度的影响
*****************************************************

*导入数据
use bbsw.dta, clear

****************
* 1. 描述性统计
****************
tabulate lifesat // lifesat的分布情况
histogram lifesat, discrete percent addlabel
graph export lifesat.png
describe lifesat kidage01 age lhinc work  // 变量描述

****************
* 2. 数据处理
****************
* 生成dummy变量
forvalues i=0/4{
gen kidage01_`i' = 0 if kidage01!=.
replace kidage01_`i' = 1 if kidage01==`i'
}

* 将work转化为dummy变量
rename work work1
gen work = 0
replace work = 1 if work1 > 0

*******************
* 3. feologit 估计
*******************
xtset idpers // 表明面板数据

* BUC estimator
feologit lifesat kidage01_0-kidage01_4 age lhinc work

* 胜算比形式
feologit, or nolog

* 边际效应
logitmarg, dydx(kidage01_0)

* BUC-tau estimator
feologit lifesat kidage01_0-kidage01_4 age lhinc work, group(idpers) threshold

*******************
* 4. 假设检验
*******************
* first step
quietly: feologit lifesat kidage01_0-kidage01_4 age lhinc work, group(idpers) threshold keep

* second step
local interact=""
foreach i of var kidage01_0-kidage01_4 age lhinc work {
quietly: generate tau_`i' =`i'*(bucsample==0)
local interact="`interact' tau_`i'"
}

* third step
clogit dkdepvar kidage01_0-kidage01_4 age lhinc work `interact' i.dkthreshold, group(clonegroup) cluster(idpers)

test `interact'