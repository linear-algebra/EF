## 实证金融 (Empirical Finance) 课程主页


### &#x1F34E; 作业发布
- &#x1F449;  [【点击查看作业】](https://gitee.com/arlionn/EF/wikis/%E6%8F%90%E4%BA%A4%E6%96%B9%E5%BC%8F%E5%92%8C%E6%97%B6%E7%82%B9.md?sort_id=1628096) 
  - ${\color{red}{New}}$  HW01 已经发布，提交截止时间：`2021/9/25 23:59`
  - HW02 已发布，提交截止时间：`2021/xx/xx 23:59` (本次作业可以两人组队完成) 
  - HW03 已发布，提交截止时间：`2020/xx/xx 23:59` (本次作业可以两人组队完成)
 

&emsp;

### &#x1F353; 交作业入口
> &#x1F449; **【[点击提交](https://workspace.jianguoyun.com/inbox/collect/8b71e5254a0f4f36ac95b582ffd7f2db/submit)】**，个人作业和期末课程报告均通过此处提交 


> Notes：
> - [1] 若需更新，只需将同名文件再次提交即可。
> - [2] 提交截止时间为 deadline 当日 23:59，逾期不候。


### 课程报告
> Update: `2021/9/15 13:36`  
- &#x1F34F; 课程报告提纲和任何阶段性文稿都可以随时通过 &#x1F449; **【[交作业入口](https://workspace.jianguoyun.com/inbox/collect/8b71e5254a0f4f36ac95b582ffd7f2db/submit)】** 提交，我看到后会联系你，以便及时修改优化。
  - 提交时的标题请按照 **【[交作业入口](https://workspace.jianguoyun.com/inbox/collect/8b71e5254a0f4f36ac95b582ffd7f2db/submit)】** 页面中的格式要求填写。
  - 课程报告格式要求：参见 [课程报告格式要求](https://www.lianxh.cn/news/3f40bb6c25f6a.html)
  - 报告范本参见：
    - [格式范本1-纯文字-稳健性检验](https://gitee.com/arlionn/EF/wikis/%E6%A0%BC%E5%BC%8F%E8%8C%83%E6%9C%AC1-%E7%BA%AF%E6%96%87%E5%AD%97-%E7%A8%B3%E5%81%A5%E6%80%A7%E6%A3%80%E9%AA%8C.md?sort_id=3213673)
    - [格式范本2-图形-公式-分位数回归中的样本自选择问题](https://gitee.com/arlionn/EF/wikis/%E6%A0%BC%E5%BC%8F%E8%8C%83%E6%9C%AC2-%E5%9B%BE%E5%BD%A2-%E5%85%AC%E5%BC%8F-%E5%88%86%E4%BD%8D%E6%95%B0%E5%9B%9E%E5%BD%92%E4%B8%AD%E7%9A%84%E6%A0%B7%E6%9C%AC%E8%87%AA%E9%80%89%E6%8B%A9%E9%97%AE%E9%A2%98.md?sort_id=3214194)
- 课程报告截止时间：`2022.01.15` 


&emsp;

### 课程报告选题 - 待更新
> `2021/9/15 13:34`   
  
大家好，我在 [课程报告 - 备选主题](https://gitee.com/arlionn/EF/wikis/README.md?sort_id=3029983) 中添加了一些期末课程论文的选题，随后会陆续增加。   
- **注意：** &#x1F34E; 认领某个选题后，请在该选题前面加上【OK-姓名-】前缀，如 [【OK-连玉君-推文标题.md】](https://gitee.com/arlionn/EF/wikis/%E9%A2%86-%E8%BF%9E%E7%8E%89%E5%90%9B-%E4%B8%80%E4%B8%AA%E6%8E%A8%E6%96%87%E6%A0%87%E9%A2%98.md?sort_id=3213361)。     
- 新增选题存放于 [【课程报告备选主题】](https://gitee.com/arlionn/EF/wikis/readme.md?sort_id=3029983) 中
  - 10.30 日之前的选题查询：[New-2020-11月选题A](https://gitee.com/Stata002/StataSX2018/wikis/%E5%B7%A5%E5%85%B7%E5%8F%98%E9%87%8F%E5%90%88%E7%90%86%E6%80%A7%E6%A3%80%E9%AA%8C-%E8%AF%86%E5%88%AB%E4%B8%8D%E8%B6%B3%E5%92%8C%E8%BF%87%E5%BA%A6%E8%AF%86%E5%88%AB.md?sort_id=2989144)；[New-2020-11月选题B](https://gitee.com/Stata002/StataSX2018/wikis/00_%E8%AE%BA%E6%96%87%E9%87%8D%E7%8E%B0%E9%93%BE%E6%8E%A5.md?sort_id=2199891)
  - 注意：请勿再从上述两个网址下选题，均以分配完毕
 

<br>
<br>

<br>
<br>


&emsp;


### 课件和 FAQs
- **课件下载：**
  - FTP 地址：ftp://ftp.lingnan.sysu.edu.cn/ &rarr; [2020_实证金融] 文件夹。
  - 账号 = 密码 = lianyjst
  - 请保护版权，未经本人同意，请勿散布于网络。
- **[FAQs](https://gitee.com/arlionn/PX/wikis/%E8%BF%9E%E7%8E%89%E5%90%9B-Stata%E5%AD%A6%E4%B9%A0%E5%92%8C%E5%90%AC%E8%AF%BE%E5%BB%BA%E8%AE%AE.md?sort_id=2662737)** 学习过程中的多数问题，都可以在这里找到答案。亦可访问 [连享会主页](https://www.lianxh.cn) 和 [连享会知乎](https://www.zhihu.com/people/arlionn/) 查阅相关推文。 

